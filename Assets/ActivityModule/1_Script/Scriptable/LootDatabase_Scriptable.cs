using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enum;
using Enumerator;
[CreateAssetMenu(fileName = "Loot", menuName = "Loot/Create New Loot", order = 1)]
public class LootDatabase_Scriptable : ScriptableObject
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====
    //===== PUBLIC =====
    public string m_LootID;
    public string m_LootName;
    public string m_LootDes;
    public Sprite m_LootImage;
    public e_Rarity m_Rarity;
    public e_TypeNode m_Type;
    public int m_Size;
    public int m_Price;
    // public bool m_FirstTime;
}
//===== PRIVATES =====
//=====================================================================
//				    OTHER METHOD
//=====================================================================

