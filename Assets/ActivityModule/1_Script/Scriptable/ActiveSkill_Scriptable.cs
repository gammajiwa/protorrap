using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(fileName ="Active Skill", menuName = "Skill/Active Skill",order = 1)]
public class ActiveSkill_Scriptable : Skill_Scriptable{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ActiveSkill_Scriptable m_Instance;
    //===== STRUCT =====

    //===== PUBLIC =====    
    public override e_SkillType m_SkillType {
        get {
            return e_SkillType.ACTIVE;
        }
    }
    //===== PRIVATES =====

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_UseSkill(int p_Level) {
        base.f_UseSkill(p_Level);
    }
}
