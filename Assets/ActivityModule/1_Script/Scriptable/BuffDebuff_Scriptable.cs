using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[CreateAssetMenu(fileName = "Buff", menuName = "Buff|Debuff/Create New Buff|Debuff", order = 1)]
public class BuffDebuff_Scriptable : ScriptableObject {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    [Serializable]
    public class c_BuffAttribute{
        public List<Attribute_Class> m_Attribute;
    }
    //===== PUBLIC =====
    public string m_BuffID;
    public List<c_BuffAttribute> m_AffectedAttributes;
    public float m_Duration = -1;
    //===== PRIVATES =====
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public List<Attribute_Class> f_GetAffectedBuff(int p_Level) {
        return m_AffectedAttributes[p_Level].m_Attribute;
    }
}
