using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(fileName = "Passive Skill", menuName = "Skill/Passive Skill", order = 2)]
public class PassiveSkill_Scriptable : Skill_Scriptable {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====
    //===== PUBLIC =====
    public override e_SkillType m_SkillType {
        get {
            return e_SkillType.PASSIVE;
        }
    }
    //===== PRIVATES =====

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public override void f_UseSkill(int p_Level) {
        base.f_UseSkill(p_Level);
    }
}
