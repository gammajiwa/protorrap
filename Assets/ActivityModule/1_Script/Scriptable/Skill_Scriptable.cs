using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Skill_Scriptable:ScriptableObject {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====
    public enum e_SkillType { 
        PASSIVE = 0,
        ACTIVE = 1
    }
    //===== PUBLIC =====
    public string m_SkillName;
    public string m_SkillID;
    public SkillEffect_Class m_SkillEffect;
    public float m_Cooldown = -1;
    public virtual e_SkillType m_SkillType {
        get;
    }
    //===== PRIVATES =====
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public virtual void f_UseSkill(int p_Level) {
        m_SkillEffect.f_SkillEffect(p_Level);
    }
}