using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Enum
{
    public enum e_MasteryType
    {
        FARMING = 1,
        FISHING = 2,
        MINING = 3,
        CARPENTRY,
        BREEDING
    }

    public enum e_AttributeType
    {
        STRENGTH = 0,
        AGILITY = 1,
        INTELIGENCE = 2,
        ASPD = 3,
        DOUBLEORE = 4,
        TRIPLEORE = 5,
        QUADRUPLEORE = 6,
        DAMAGE = 7,
        CURRENCY = 8,
        CRITICAL = 9,
        ORERARITY = 10,
        ORERARITY2 = 11,
        ORERARITY3 = 12,
        RUPYCURRENCY = 13,
        TIMER = 14,
        HARVEST = 15,
        KILL = 16,
        BAITLEVEL = 17,
        FISHINGRODLEVEL = 18,
        MiningNodeLevel = 19,
        AXEPLODE = 20,
        DOUBLELOG = 21,
        TRIPLELOG = 22,
        QUADRUPLELOG = 23,
        CHANCELOGPLANK = 24,
        RAREDROP = 25,
        EPICDROP = 26,
        EPICDROP2 = 27,
        LEGENDARYDROP = 28,
        QUICKREEL = 29,
        FISHRUSH = 30,
        AUTOREEL = 31,
        DOUBLEWEIGHT = 32,
        TRIPLEWEIGHT = 33,
        QUADRUPLEWEIGHT = 34,
        AUTOSELL = 35,
        INSTANTCATCH = 36,
        HARVESTSAUCE = 37,
        MOREPRICEFORCROPS = 38,
        INSTAGROW = 39,






    }

    public enum e_Rarity
    {
        COMMON = 0,
        UNCOMMON = 1,
        RARE = 2,
        EPIC = 3,
        LEGENDARY = 4,
    }

    public enum e_DamageType
    {
        NONE = 0,
        NORMAL = 1,
        FIRE = 2,
    }
}
