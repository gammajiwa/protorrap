using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResultDrop_Class {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public List<Result_Class> m_Results = new List<Result_Class>();
    //===== PRIVATES =====
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public ResultDrop_Class(List<Result_Class> p_Results) {
        m_Results.Clear();
        m_Results.AddRange(p_Results);
    }
}
