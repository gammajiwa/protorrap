using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Formulas;

[Serializable]
public class LootData_Class{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public List<LootInfo_Class> m_LootInfoClass;
    public bool m_IsBranching;
    public List<LootData_Class> m_LootDataClass;
    public float m_Weight;
    //===== PRIVATES =====
    private List<Result_Class> t_Results = new List<Result_Class>();
    private List<float> t_Probabilty = new List<float>();
    private int t_Index = 0;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public ResultDrop_Class f_GetLootResult(int p_LootCount) {
        t_Probabilty.Clear();

        if (!m_IsBranching)
        {
            t_Results.Clear();
            for (int i = 0; i < m_LootInfoClass.Count; i++) {
                t_Probabilty.Add(m_LootInfoClass[i].m_DropWeight);
            }

            for (int i = 0; i < p_LootCount; i++) {
                t_Results.Add(m_LootInfoClass[Formula.f_FormulaDistributionGacha(t_Probabilty)].f_ReturnLootResult());
            }

            return new ResultDrop_Class(t_Results);
        }
        else {
            for (int i = 0; i < m_LootDataClass.Count; i++)
            {
                t_Probabilty.Add(m_LootDataClass[i].m_Weight);
            }

            t_Index = Formula.f_FormulaDistributionGacha(t_Probabilty);

            return m_LootDataClass[t_Index].f_GetLootResult(p_LootCount);
        }
    }
}
