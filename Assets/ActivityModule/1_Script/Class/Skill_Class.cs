using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[Serializable]
public class Skill_Class {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====

    //===== PUBLIC =====
    public int m_Level = 0;
    public List<int> m_RequiredLevel = new List<int>();
    public Skill_Scriptable m_SkillData;
    //===== PRIVATES =====
    private float m_CurrentCooldown = 0;
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_LevelUp() {
        m_Level++;
        if (m_SkillData.m_SkillType == Skill_Scriptable.e_SkillType.PASSIVE) m_SkillData.f_UseSkill(m_Level - 1);
        else {
            PlayerAttribute.m_Instance.m_Skill = this;
        }
    }

    public void f_UseSkill() {
        if (m_SkillData.m_SkillType == Skill_Scriptable.e_SkillType.ACTIVE) {
            if (m_CurrentCooldown <= 0)
            {
                m_CurrentCooldown = m_SkillData.m_Cooldown;
                m_SkillData.f_UseSkill(m_Level - 1);
            }
            else {
                Debug.Log("Skill On Cooldown");
            }
        }
    }

    public void f_CalculateCooldown() {
        if(m_CurrentCooldown>0) m_CurrentCooldown = Mathf.Max(m_CurrentCooldown-Time.fixedDeltaTime,0);
    }
}
