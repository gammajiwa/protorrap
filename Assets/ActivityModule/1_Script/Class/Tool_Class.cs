using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
[Serializable]
public class Tool_Class {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_ToolsID;
    public BuffDebuff_Scriptable m_ToolsBuff;
    public List<float> m_RequiredGold;
    public int m_Level;
    //===== PRIVATES =====

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public virtual void f_Upgrade() {
        m_Level++;
        Buff_Manager.m_Instance.f_ApplyBuff(m_ToolsBuff,m_Level-1);
    }
}
