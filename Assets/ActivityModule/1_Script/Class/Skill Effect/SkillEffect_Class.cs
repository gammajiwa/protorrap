using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SkillEffect_Class:MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    public BuffDebuff_Scriptable m_Buff;
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    //Efek Skill mengeluarkan apa, mungkin mengeluarkan Projectile atau memunculkan aura, atau mungkin dapat gold instan, semua di code di function ini setelah
    //diturunkan jadi di override
    public virtual void f_SkillEffect(int p_Level) {       
        Buff_Manager.m_Instance.f_ApplyBuff(m_Buff, p_Level);
    }
}
