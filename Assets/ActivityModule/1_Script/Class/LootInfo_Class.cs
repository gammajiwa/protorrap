using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

[Serializable]
public class LootInfo_Class {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public LootDatabase_Scriptable m_LootData;
    public float m_DropWeight;
    public int m_MaxAmount;
    public int m_MinAmount;
    //===== PRIVATES =====

    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public Result_Class f_ReturnLootResult() {
        return new Result_Class(m_LootData, Random.Range(m_MinAmount, m_MaxAmount));
    }
}
