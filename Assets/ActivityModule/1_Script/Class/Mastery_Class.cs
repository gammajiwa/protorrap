using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enum;
[Serializable]
public class Mastery_Class {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====
    //===== PUBLIC =====
    public string m_MasteryName;
    public List<Skill_Class> m_Skills = new List<Skill_Class>();
    public List<e_MasteryType> m_MasteryTypes;
    public int m_Level = 1;
    public float m_Experience = 0;
    public float m_RequiredExp = 1000;
    //===== PRIVATES =====
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_CalculateExp(float p_Exp) {
        m_Experience += p_Exp;
        while(m_Experience>=m_RequiredExp){
            m_Experience -= m_RequiredExp;
            m_Level++;

            for (int i = 0; i < m_Skills.Count; i++) {
                for (int j = 0; j < m_Skills[i].m_RequiredLevel.Count; j++) {
                    if (m_Skills[i].m_RequiredLevel[j] == m_Level) {
                        m_Skills[i].f_LevelUp();
                        break;
                    }
                }
            }
        }
    }

    public void f_CalculateSkillCooldown() { 
        for(int i = 0; i < m_Skills.Count; i++)
        {
            if (m_Skills[i].m_SkillData.m_SkillType == Skill_Scriptable.e_SkillType.ACTIVE) {
                m_Skills[i].f_CalculateCooldown();
            }
        }
    }

}
