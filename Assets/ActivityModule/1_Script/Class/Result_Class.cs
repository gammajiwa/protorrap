using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Result_Class{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====
    //===== PUBLIC =====
    public LootDatabase_Scriptable m_LootData;
    public int m_Amount;
    //===== PRIVATES =====

    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public Result_Class(LootDatabase_Scriptable p_LootData,int p_Amount) {
        m_Amount = p_Amount;
        m_LootData = p_LootData;
    }
}
