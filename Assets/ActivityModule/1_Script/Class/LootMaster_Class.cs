using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enum;
using Formulas;
using Random = UnityEngine.Random;
[Serializable]
public class LootMaster_Class {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    //===== STRUCT =====

    //===== PUBLIC =====
    public string m_LootCode; 
    public e_DamageType m_RequiredDamageType;
    public int m_DropAmount = 1;
    public int m_MaxDropAmount = 2;
    public int m_MinDropAmount = 1;
    public List<LootData_Class> m_ResultDrop;    
    //===== PRIVATES =====
    private ResultDrop_Class t_ResultDrop;
    private List<float> t_Probabiltys = new List<float>();
    int t_Index = 0;
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public ResultDrop_Class f_GetLootDrop() {
        if (m_ResultDrop.Count == 1)
        {
            t_ResultDrop = m_ResultDrop[0].f_GetLootResult(Random.Range(m_MinDropAmount,m_MaxDropAmount));
        }
        else if (m_ResultDrop.Count > 1) {
            if (t_Probabiltys.Count == 0) {
                for (int i = 0; i < m_ResultDrop.Count; i++) {
                    t_Probabiltys.Add(m_ResultDrop[i].m_Weight);
                }
            }
            t_Index = Formula.f_FormulaDistributionGacha(t_Probabiltys);
            t_ResultDrop = m_ResultDrop[t_Index].f_GetLootResult(Random.Range(m_MinDropAmount, m_MaxDropAmount));
        }

        return t_ResultDrop;
    }
}
