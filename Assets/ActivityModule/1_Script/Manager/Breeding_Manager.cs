using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
public class Breeding_Manager : Activity_Manager
{
    public override void f_Action(string p_Name = "", e_DamageType p_DamageType = e_DamageType.NONE)
    {
        m_Mastery.f_CalculateExp(100);

        for (int i = 0; i < m_LootDrops.Count; i++) {
            if (string.Compare(m_LootDrops[i].m_LootCode,p_Name) == 0) {
                t_Index = i;
                break;
            }
        }

        t_ResultDrop = m_LootDrops[t_Index].f_GetLootDrop();

        for (int i = 0; i < t_ResultDrop.m_Results.Count; i++)
        {
            Game_Manager.m_Instance.f_ItemDrops(Breeding_Station.m_Instance.transform.position, t_ResultDrop.m_Results[i].m_LootData.m_LootName, c_QuestGoal.e_Goaltype.Mob,t_ResultDrop.m_Results[i].m_Amount);
        }
    }
}
