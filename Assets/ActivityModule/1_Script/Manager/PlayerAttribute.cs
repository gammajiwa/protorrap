using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enum;
public class PlayerAttribute : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static PlayerAttribute m_Instance;
    //===== STRUCT =====
    //===== PUBLIC =====
    public List<Attribute_Class> m_BaseAttribute;
    public List<Attribute_Class> m_AddedAttribute;
    public Skill_Class m_Skill;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {

    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public float f_GetFinalAttribute(e_AttributeType p_Type)
    {
        for (int i = 0; i < m_BaseAttribute.Count; i++)
        {
            if (m_BaseAttribute[i].m_AttributeType == p_Type)
            {
                for (int j = 0; j < m_AddedAttribute.Count; j++)
                {
                    if (m_AddedAttribute[j].m_AttributeType == p_Type) return (int)m_BaseAttribute[i].m_Amount + (int)m_AddedAttribute[j].m_Amount;
                }
            }
        }

        return 0;
    }
}
