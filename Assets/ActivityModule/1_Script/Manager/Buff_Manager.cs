using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Buff_Manager : MonoBehaviour {
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static Buff_Manager m_Instance;
    //===== STRUCT =====
    [Serializable]
    public class c_BuffDebuff{
        public string m_BuffID;
        public List<Attribute_Class> m_AffectedAttribute = new List<Attribute_Class>();
        public float m_Duration;

        public c_BuffDebuff(string p_BuffID,List<Attribute_Class> p_Attributes, float p_Duration) {
            m_BuffID = p_BuffID;
            m_AffectedAttribute.AddRange(p_Attributes);
            m_Duration = p_Duration;
            f_ApplyBuff();
        }

        public void f_UpdateBuff(List<Attribute_Class> p_Attributes) {
            f_ReleaseBuff();
            m_AffectedAttribute.Clear();
            m_AffectedAttribute.AddRange(p_Attributes);
            f_ApplyBuff();
        }

        public void f_ApplyBuff() {
            for (int i = 0; i < m_AffectedAttribute.Count; i++) {
                for (int j = 0; j < PlayerAttribute.m_Instance.m_AddedAttribute.Count; j++) {
                    if (m_AffectedAttribute[i].m_AttributeType == PlayerAttribute.m_Instance.m_AddedAttribute[j].m_AttributeType) {
                        PlayerAttribute.m_Instance.m_AddedAttribute[j].m_Amount += m_AffectedAttribute[i].m_Amount;
                        break;
                    }
                }
            }
        }

        public void f_ReleaseBuff() {
            for (int i = 0; i < m_AffectedAttribute.Count; i++) {
                for (int j = 0; j < PlayerAttribute.m_Instance.m_AddedAttribute.Count; j++) {
                    if (m_AffectedAttribute[i].m_AttributeType == PlayerAttribute.m_Instance.m_AddedAttribute[j].m_AttributeType) {
                        PlayerAttribute.m_Instance.m_AddedAttribute[j].m_Amount -= m_AffectedAttribute[i].m_Amount;
                        break;
                    }
                }
            }
        }
    }
    //===== PUBLIC =====
    public List<c_BuffDebuff> m_BuffDebuffList;
    //===== PRIVATES =====
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake(){
        m_Instance = this;
    }

    void Start(){
        
    }

    void Update(){

    }

    private void FixedUpdate()
    {
        for (int i = m_BuffDebuffList.Count - 1; i > 0; i--)
        {
            if (m_BuffDebuffList[i].m_Duration > -1)
            {
                m_BuffDebuffList[i].m_Duration -= Time.fixedDeltaTime;
                if (m_BuffDebuffList[i].m_Duration <= 0)
                {
                    m_BuffDebuffList[i].f_ReleaseBuff();
                    m_BuffDebuffList.Remove(m_BuffDebuffList[i]);
                }
            }
        }
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_ApplyBuff(BuffDebuff_Scriptable p_Buff,int p_Level) {
        bool t_NotFound = true;
        for (int i = 0; i < m_BuffDebuffList.Count; i++) {
            if (string.Compare(m_BuffDebuffList[i].m_BuffID,p_Buff.m_BuffID) == 0) {
                m_BuffDebuffList[i].f_UpdateBuff(p_Buff.f_GetAffectedBuff(p_Level));
                t_NotFound = false;
                break;
            }
        }

        if (t_NotFound) {
            f_AddNewBuff(p_Buff,p_Level);
        }
    }

    public void f_AddNewBuff(BuffDebuff_Scriptable p_Buff, int p_Level) {
        c_BuffDebuff t_NewBuff = new c_BuffDebuff(p_Buff.m_BuffID,p_Buff.f_GetAffectedBuff(p_Level),p_Buff.m_Duration);
        m_BuffDebuffList.Add(t_NewBuff);
    }
}
