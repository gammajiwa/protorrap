using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enum;

public class Activity_Manager : MonoBehaviour{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== STRUCT =====
    public static Activity_Manager m_Instance;
    //===== PUBLIC =====
    public Mastery_Class m_Mastery;
    public List<Tool_Class> m_Tools;
    public List<LootMaster_Class> m_LootDrops;
    //===== PRIVATES =====
    protected ResultDrop_Class t_ResultDrop;
    protected int t_Index = 0;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    public void FixedUpdate()
    {
        m_Mastery.f_CalculateSkillCooldown();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================
    public void f_UpgradeTools(int p_ToolsIndex) {
        m_Tools[p_ToolsIndex].f_Upgrade();
    }

    public virtual void f_Action(string p_Name = "",e_DamageType p_DamageType = e_DamageType.NONE) { 
        
    }

    public void f_UseSkill(string p_SkillID) {
        for (int i = 0; i < m_Mastery.m_Skills.Count;i++) {
            if (string.Compare(p_SkillID, m_Mastery.m_Skills[i].m_SkillData.m_SkillID) == 0) {
                m_Mastery.m_Skills[i].f_UseSkill();
            }
        }
    }
}
