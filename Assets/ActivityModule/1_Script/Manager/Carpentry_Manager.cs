using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
using Formulas;
public class Carpentry_Manager : Activity_Manager
{
    private float t_DoubleDropChance;
    private float t_TripleDropChance;
    private float t_QuadDropChance;

    public override void f_Action(string p_Name = "",e_DamageType p_DamageType = e_DamageType.NONE)
    {
        m_Mastery.f_CalculateExp(100);

        for (int i = 0; i < m_LootDrops.Count; i++)
        {
            if (string.Compare(m_LootDrops[i].m_LootCode, p_Name) == 0)
            {
                t_Index = i;
                break;
            }
        }

        t_ResultDrop = m_LootDrops[t_Index].f_GetLootDrop();

        t_DoubleDropChance = PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.DOUBLELOG);
        t_TripleDropChance = PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.TRIPLELOG);
        t_QuadDropChance = PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.QUADRUPLELOG);

        for (int i = 0; i < t_ResultDrop.m_Results.Count; i++)
        {
            if (t_QuadDropChance > 0)
            {
                if (Formula.f_RandomDistributionGacha(t_QuadDropChance, 10))
                {
                    t_ResultDrop.m_Results[i].m_Amount *= 4;
                }
            }
            else if (t_TripleDropChance > 0)
            {
                if (Formula.f_RandomDistributionGacha(t_TripleDropChance, 10))
                {
                    t_ResultDrop.m_Results[i].m_Amount *= 3;
                }
            }
            else if (t_DoubleDropChance > 0)
            {
                if (Formula.f_RandomDistributionGacha(t_DoubleDropChance, 10))
                {
                    t_ResultDrop.m_Results[i].m_Amount *= 2;
                }
            }
            Game_Manager.m_Instance.f_ItemDrops(GameObject_Node.m_Instance.transform.position, t_ResultDrop.m_Results[i].m_LootData.m_LootName, c_QuestGoal.e_Goaltype.Logs,t_ResultDrop.m_Results[i].m_Amount);
        }
    }
}
