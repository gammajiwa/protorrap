using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
using Formulas;
public class Mining_Manager : Activity_Manager
{
    private float t_DoubleDropChance;
    private float t_TripleDropChance;
    private float t_QuadDropChance;

    public override void f_Action(string p_Name = "",e_DamageType p_DamageType = e_DamageType.NONE)
    {
        m_Mastery.f_CalculateExp(100);

        t_Index = (int) PlayerAttribute.m_Instance.f_GetFinalAttribute( e_AttributeType.MiningNodeLevel);

        t_ResultDrop = m_LootDrops[t_Index].f_GetLootDrop();

        t_DoubleDropChance = PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.DOUBLEORE);
        t_TripleDropChance = PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.TRIPLEORE);
        t_QuadDropChance = PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.QUADRUPLEORE);

        for (int i = 0; i < t_ResultDrop.m_Results.Count; i++) {
            if (t_QuadDropChance > 0)
            {
                if (Formula.f_RandomDistributionGacha(t_QuadDropChance, 10)) {
                    t_ResultDrop.m_Results[i].m_Amount *= 4;
                }
            }
            else if (t_TripleDropChance > 0)
            {
                if (Formula.f_RandomDistributionGacha(t_TripleDropChance, 10))
                {
                    t_ResultDrop.m_Results[i].m_Amount *= 3;
                }
            }
            else if (t_DoubleDropChance > 0) {
                if (Formula.f_RandomDistributionGacha(t_DoubleDropChance, 10))
                {
                    t_ResultDrop.m_Results[i].m_Amount *= 2;
                }
            }

            Game_Manager.m_Instance.f_ItemDrops(GameObject_Node.m_Instance.transform.position, t_ResultDrop.m_Results[i].m_LootData.m_LootName, c_QuestGoal.e_Goaltype.Mineral, t_ResultDrop.m_Results[i].m_Amount);
        }

        
    }
}
