using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
public class Fishing_Manager : Activity_Manager
{
    public override void f_Action(string p_Name = "",e_DamageType p_DamageType = e_DamageType.NONE)
    {
        m_Mastery.f_CalculateExp(100);

        t_Index = (int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.BAITLEVEL);

        t_ResultDrop =  m_LootDrops[t_Index].f_GetLootDrop();

        for (int i = t_ResultDrop.m_Results.Count - 1; i > 0; i--)
        {
            if ((int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.FISHINGRODLEVEL) == 0)
            {
                if ((int)t_ResultDrop.m_Results[i].m_LootData.m_Rarity>1) {
                    t_ResultDrop.m_Results.Remove(t_ResultDrop.m_Results[i]);
                }
            }
            else if ((int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.FISHINGRODLEVEL) == 1)
            {
                if ((int)t_ResultDrop.m_Results[i].m_LootData.m_Rarity >2)
                {
                    t_ResultDrop.m_Results.Remove(t_ResultDrop.m_Results[i]);
                }
            }
            else if ((int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.FISHINGRODLEVEL) == 2)
            {
                if ((int)t_ResultDrop.m_Results[i].m_LootData.m_Rarity >3)
                {
                    t_ResultDrop.m_Results.Remove(t_ResultDrop.m_Results[i]);
                }
            }
        }

        for (int i = 0; i < t_ResultDrop.m_Results.Count; i++) {
            Game_Manager.m_Instance.f_ItemDrops(new Vector3 (0,-4,0), t_ResultDrop.m_Results[i].m_LootData.m_LootName, c_QuestGoal.e_Goaltype.Fish, t_ResultDrop.m_Results[i].m_Amount);
        }
    }
}
