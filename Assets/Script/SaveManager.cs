using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }





    // SaveData f_SaveGame()
    // {
    //     SaveData m_Save = new SaveData();


    //     // m_Save.m_Coin = PlayerStat.instance.m_CoinPlayer;
    //     // m_Save.m_WeaponOwned = PlayerStat.instance.m_OwnedWeapon;
    //     // m_Save.m_9mm = PlayerStat.instance.m_Ammo9mm;
    //     // m_Save.m_55mm = PlayerStat.instance.m_Ammo55mm;
    //     // m_Save.m_12gauge = PlayerStat.instance.m_Ammo12AGauge;
    //     // m_Save.m_CurrUnlockStage = LevelStageManager.instance.m_UnlockStage;
    //     // m_Save.m_FirstLogin = GameManager.instance.m_FirstLogin;




    //     // for (int i = 0; i < LevelStageManager.instance.m_AllStart.Count; i++)
    //     // {
    //     //     m_Save.m_StarStage.Add(LevelStageManager.instance.m_AllStart[i]);

    //     // }
    //     // for (int i = 0; i < PlayerStat.instance.m_ListWeapons.Count; i++)
    //     // {
    //     //     m_Save.m_MyWeaponsAmmo.Add(PlayerStat.instance.m_ListWeapons[i].m_CurrentAmmo);
    //     // }

    //     // return m_Save;
    // }


    // public void f_SaveByJSON()
    // {
    //     SaveData m_Save = f_SaveGame();

    //     string m_JsonString = JsonUtility.ToJson(m_Save);
    //     StreamWriter m_Sw = new StreamWriter(Application.persistentDataPath + "/SaveJSON.text");
    //     m_Sw.Write(m_JsonString);
    //     m_Sw.Close();
    //     Debug.Log("SaveData");
    // }


    // public void f_LoadByJSON()
    // {
    //     if (File.Exists(Application.persistentDataPath + "/SaveJSON.text"))
    //     {
    //         StreamReader m_Sr = new StreamReader(Application.persistentDataPath + "/SaveJSON.text");
    //         string m_JsonString = m_Sr.ReadToEnd();
    //         m_Sr.Close();


    //         SaveData m_Save = JsonUtility.FromJson<SaveData>(m_JsonString);
    //         Debug.Log("Load Data");

    //         PlayerStat.instance.m_CoinPlayer = m_Save.m_Coin;
    //         PlayerStat.instance.m_OwnedWeapon = m_Save.m_WeaponOwned;
    //         PlayerStat.instance.m_Ammo9mm = m_Save.m_9mm;
    //         PlayerStat.instance.m_Ammo55mm = m_Save.m_55mm;
    //         PlayerStat.instance.m_Ammo12AGauge = m_Save.m_12gauge;
    //         LevelStageManager.instance.m_UnlockStage = m_Save.m_CurrUnlockStage;
    //         GameManager.instance.m_FirstLogin = m_Save.m_FirstLogin;

    //         for (int i = 0; i < m_Save.m_StarStage.Count; i++)
    //         {
    //             LevelStageManager.instance.m_AllStart[i] = m_Save.m_StarStage[i];
    //         }
    //         for (int i = 0; i < m_Save.m_MyWeaponsAmmo.Count; i++)
    //         {
    //             PlayerStat.instance.m_ListWeapons[i].m_CurrentAmmo = m_Save.m_MyWeaponsAmmo[i];
    //         }

    //     }
    //     else
    //     {
    //         Debug.Log("Not Found File");
    //     }
    // }

}
