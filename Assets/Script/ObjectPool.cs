using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string m_Tag;
        public int m_Size;
        public GameObject m_Prefab;
        public Transform m_Container;
    }


    public List<Pool> m_Pools;
    public Dictionary<string, Queue<GameObject>> m_PoolDictionary;

    #region Singleton
    public static ObjectPool m_Instance;
    private void Awake()
    {
        m_Instance = this;
    }

    #endregion


    void Start()
    {
        m_PoolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool m_Pool in m_Pools)
        {
            Queue<GameObject> m_ObjectPool = new Queue<GameObject>();

            for (int i = 0; i < m_Pool.m_Size; i++)
            {
                GameObject m_Obj = Instantiate(m_Pool.m_Prefab);
                m_Obj.transform.parent = m_Pool.m_Container;
                m_Obj.SetActive(false);
                m_ObjectPool.Enqueue(m_Obj);
            }
            m_PoolDictionary.Add(m_Pool.m_Tag, m_ObjectPool);
        }
    }

    public GameObject f_SpanwFromPool(string p_Tag, Vector3 p_Position, Quaternion p_Rotation)
    {
        if (!m_PoolDictionary.ContainsKey(p_Tag))
        {
            return null;
        }
        GameObject m_ObjToSpawn = m_PoolDictionary[p_Tag].Dequeue();
        m_ObjToSpawn.SetActive(true);
        m_ObjToSpawn.transform.position = p_Position;
        m_ObjToSpawn.transform.rotation = p_Rotation;

        m_PoolDictionary[p_Tag].Enqueue(m_ObjToSpawn);

        return m_ObjToSpawn;

    }

}
