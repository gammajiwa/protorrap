using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Manager : MonoBehaviour
{

    public static Skill_Manager m_Instance;
    // Start is called before the first frame update


    private void Awake()
    {
        m_Instance = this;
    }


    public void f_UseSkill(string p_SkillID)
    {
        Activity_Manager.m_Instance.f_UseSkill(p_SkillID);
    }
}
