using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Foodpedia : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> m_FoodexBar;
    public TextMeshProUGUI m_Price;
    public TextMeshProUGUI m_Des;
    public TextMeshProUGUI m_NameFood;
    public Image m_Image;

    public int m_Index;


    private void Awake()
    {

        f_UpdateFoodPedia();
        Inventory.m_Instance.FoodPediaUpdateCallBack += f_UpdateFoodPedia;

    }


    public void f_UpdateFoodPedia()
    {
        foreach (var item in m_FoodexBar)
        {
            item.GetComponentInChildren<TextMeshProUGUI>().text = "";

        }
        for (int i = 0; i < Inventory.m_Instance.m_Items.Count; i++)
        {
            m_FoodexBar[i].GetComponentInChildren<TextMeshProUGUI>().text = Inventory.m_Instance.m_Items[i].m_LootName;
            foreach (var item in Inventory.m_Instance.m_FoodPedia)
            {
                if (m_FoodexBar[i].GetComponentInChildren<TextMeshProUGUI>().text == item.m_LootName)
                {
                    m_FoodexBar[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.red;
                }

            }

            // if (!Inventory.m_Instance.m_Items[i].m_FirstTime)
            // {
            //     m_FoodexBar[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.red;

            // }


        }
    }

    private void OnDisable()
    {
        Inventory.m_Instance.FoodPediaUpdateCallBack -= f_UpdateFoodPedia;
    }





    public void f_Clear()
    {

    }
}
