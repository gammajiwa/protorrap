using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using GoogleMobileAds.Api;
using System;
using Enumerator;
using Yodo1.MAS;

public class AdScript : MonoBehaviour
{
    public static AdScript m_Instance;
    // // InterstitialAd m_InterstitialAd;
    // RewardedAd m_RewardedAd;
    // // BannerView m_Banner;
    // string m_InterstitialID;
    // string m_RewardedID;
    // string m_BannerID;






    private void Awake()
    {
        m_Instance = this;

    }


    void Start()
    {
        // m_InterstitialID = "ca-app-pub-3940256099942544/1033173712";
        // m_RewardedID = "ca-app-pub-3940256099942544/5224354917";
        // m_BannerID = "ca-app-pub-3940256099942544/6300978111";

        // MobileAds.Initialize(initStatus => { });
        // f_RequestInterstitialAd();
        // f_RequestRewardedAd();
        // f_RequestBanner();

        InitializeRewardedAds();

    }



    void f_RequestBanner()
    {
        // m_Banner = new BannerView(m_BannerID, AdSize.SmartBanner, AdPosition.Bottom);

        // AdRequest request = new AdRequest.Builder().Build();
        // this.m_InterstitialAd.LoadAd(request);
    }
    void f_RequestInterstitialAd()
    {
        // m_InterstitialAd = new InterstitialAd(m_InterstitialID);


        // m_InterstitialAd.OnAdLoaded += HandleOnAdLoaded;

        // m_InterstitialAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;

        // m_InterstitialAd.OnAdOpening += HandleOnAdOpening;

        // m_InterstitialAd.OnAdClosed += HandleOnAdClosed;



        // AdRequest request = new AdRequest.Builder().Build();
        // this.m_InterstitialAd.LoadAd(request);
    }


    void f_RequestRewardedAd()
    {

        // m_RewardedAd = new RewardedAd(m_RewardedID);


        // m_RewardedAd.OnAdLoaded += HandleRewardedAdLoaded;

        // m_RewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;

        // m_RewardedAd.OnAdOpening += HandleRewardedAdOpening;

        // m_RewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;

        // m_RewardedAd.OnUserEarnedReward += HandleUserEarnedReward;

        // m_RewardedAd.OnAdClosed += HandleRewardedAdClosed;



        // AdRequest request = new AdRequest.Builder().Build();
        // this.m_RewardedAd.LoadAd(request);
    }


    public void f_ShowInterstitialAd()
    {
        // if (m_InterstitialAd.IsLoaded())
        // {
        //     // m_InterstitialAd.Show();
        //     f_RequestInterstitialAd();
        // }
    }
    public void f_ShowRewardedAd()
    {
        // if (m_RewardedAd.IsLoaded())
        // {
        //     m_RewardedAd.Show();

        // }
        Yodo1U3dMas.ShowRewardedAd();
    }


    // public void HandleOnAdLoaded(object sender, EventArgs args)
    // {

    // }
    // public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }

    // public void HandleOnAdOpening(object sender, EventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }

    // public void HandleOnAdClosed(object sender, EventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }







    // public void HandleRewardedAdLoaded(object sender, EventArgs args)
    // {

    // }

    // public void HandleRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }

    // public void HandleRewardedAdOpening(object sender, EventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }

    // public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }

    // public void HandleRewardedAdClosed(object sender, EventArgs args)
    // {
    //     f_RequestRewardedAd();
    // }

    // public void HandleUserEarnedReward(object sender, Reward args)
    // {
    //     f_RequestRewardedAd();
    //     switch ((int)Player_Data.m_Instance.m_TypeRewardAds)
    //     {
    //         case 0:
    //             Debug.Log("Player Dapat 100% Energy");
    //             Player_Data.m_Instance.m_CurrEnergy = Player_Data.m_Instance.m_MaxEnergy;
    //             Player_Data.m_Instance.m_IsSleep = false;
    //             break;
    //         case 1:
    //             Debug.Log("Dapat 2 Rupy");
    //             Player_Data.m_Instance.m_RupyCurrency += 2;
    //             UI_Manager.m_Instance.f_UpdateCurrency();
    //             break;
    //         case 2:
    //             if (!Player_Data.m_Instance.m_IsTripleLuck)
    //             {
    //                 Player_Data.m_Instance.m_LuckIndex = 2;
    //                 Debug.Log("Double Luck");
    //             }
    //             else
    //             {
    //                 Debug.Log("QuadrupleLuck");
    //                 Player_Data.m_Instance.m_LuckIndex = 4;
    //             }
    //             Player_Data.m_Instance.m_CloverAds--;
    //             Player_Data.m_Instance.m_TimerLuck += 900;
    //             UI_Manager.m_Instance.f_DisplayCloversAds();
    //             break;
    //         case 3:
    //             Debug.Log("Dapet Material Gacha");
    //             break;
    //         case 4:
    //             Debug.Log("Dapet  2x Offline Progression");
    //             break;
    //         case 5:
    //             Player_Data.m_Instance.m_ProgressiveAds++;
    //             switch (Player_Data.m_Instance.m_ProgressiveAds)
    //             {
    //                 case 1:
    //                     Debug.Log("Dapet Receh");
    //                     Player_Data.m_Instance.m_RecehCurrency += 100;
    //                     UI_Manager.m_Instance.f_UpdateCurrency();
    //                     break;
    //                 case 2:
    //                     Debug.Log("Dapet Rupy");
    //                     Player_Data.m_Instance.m_RupyCurrency += 10;
    //                     UI_Manager.m_Instance.f_UpdateCurrency();
    //                     break;
    //                 case 3:
    //                     Debug.Log("2x Random Craft Chest");
    //                     break;
    //                 case 4:
    //                     Debug.Log("Dapet Rupy");
    //                     Player_Data.m_Instance.m_RecehCurrency += 10;
    //                     UI_Manager.m_Instance.f_UpdateCurrency();
    //                     break;
    //                 case 5:
    //                     Debug.Log("Random Legendary Material");
    //                     break;
    //             }



    //             break;
    //     }
    //     Game_Manager.m_Instance.m_AdsReward.SetActive(false);
    // }


    public void f_AddAds(e_TypeRewardAds p_index)
    {
        UI_Manager.m_Instance.m_AdsReward.SetActive(true);
        Player_Data.m_Instance.m_TypeRewardAds = p_index;
        UI_Manager.m_Instance.m_DesAds.text = UI_Manager.m_Instance.m_DesTextAds[(int)p_index];
        Player_Data.m_Instance.m_TimerAds5Minute = 300;
    }



    public void f_CloverAds()
    {
        Player_Data.m_Instance.m_TypeRewardAds = e_TypeRewardAds.Luck;
        f_ShowRewardedAd();
    }


    public void f_GachaAds()
    {
        f_AddAds(e_TypeRewardAds.Gacha);
    }

    public void f_ProgressiveAds()
    {
        f_AddAds(e_TypeRewardAds.Progressive);
    }








    private void InitializeRewardedAds()
    {
        // Add Events
        Yodo1U3dMasCallback.Rewarded.OnAdOpenedEvent += OnRewardedAdOpenedEvent;
        Yodo1U3dMasCallback.Rewarded.OnAdClosedEvent += OnRewardedAdClosedEvent;
        Yodo1U3dMasCallback.Rewarded.OnAdReceivedRewardEvent += OnAdReceivedRewardEvent;
        Yodo1U3dMasCallback.Rewarded.OnAdErrorEvent += OnRewardedAdErorEvent;
    }
    private void OnRewardedAdOpenedEvent()
    {
        Debug.Log("[Yodo1 Mas] Rewarded ad opened");
    }
    private void OnRewardedAdClosedEvent()
    {
        Debug.Log("[Yodo1 Mas] Rewarded ad closed");
    }
    private void OnAdReceivedRewardEvent()
    {
        Debug.Log("[Yodo1 Mas] Rewarded ad received reward");
        switch ((int)Player_Data.m_Instance.m_TypeRewardAds)
        {
            case 0:
                Debug.Log("Player Dapat 100% Energy");
                Player_Data.m_Instance.m_CurrEnergy = Player_Data.m_Instance.m_MaxEnergy;
                Player_Data.m_Instance.m_IsSleep = false;
                break;
            case 1:
                Debug.Log("Dapat 2 Rupy");
                Player_Data.m_Instance.m_RupyCurrency += 2;
                UI_Manager.m_Instance.f_UpdateCurrency();
                break;
            case 2:
                if (!Player_Data.m_Instance.m_IsTripleLuck)
                {
                    Player_Data.m_Instance.m_LuckIndex = 2;
                    Debug.Log("Double Luck");
                }
                else
                {
                    Debug.Log("QuadrupleLuck");
                    Player_Data.m_Instance.m_LuckIndex = 4;
                }
                Player_Data.m_Instance.m_CloverAds--;
                Player_Data.m_Instance.m_TimerLuck += 900;
                UI_Manager.m_Instance.f_DisplayCloversAds();
                break;
            case 3:
                Debug.Log("Dapet Material Gacha");
                break;
            case 4:
                Debug.Log("Dapet  2x Offline Progression");
                break;
            case 5:
                Player_Data.m_Instance.m_ProgressiveAds++;
                switch (Player_Data.m_Instance.m_ProgressiveAds)
                {
                    case 1:
                        Debug.Log("Dapet Receh");
                        Player_Data.m_Instance.m_RecehCurrency += 100;
                        UI_Manager.m_Instance.f_UpdateCurrency();
                        break;
                    case 2:
                        Debug.Log("Dapet Rupy");
                        Player_Data.m_Instance.m_RupyCurrency += 10;
                        UI_Manager.m_Instance.f_UpdateCurrency();
                        break;
                    case 3:
                        Debug.Log("2x Random Craft Chest");
                        break;
                    case 4:
                        Debug.Log("Dapet Rupy");
                        Player_Data.m_Instance.m_RecehCurrency += 10;
                        UI_Manager.m_Instance.f_UpdateCurrency();
                        break;
                    case 5:
                        Debug.Log("Random Legendary Material");
                        break;
                }
                break;
        }
        UI_Manager.m_Instance.m_AdsReward.SetActive(false);
    }
    private void OnRewardedAdErorEvent(Yodo1U3dAdError adError)
    {
        Debug.Log("[Yodo1 Mas] Rewarded ad error - " + adError.ToString());
    }

}
