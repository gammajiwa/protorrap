using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Enumerator
{
    public enum e_Activities
    {
        Register,
        Mining,
        Carpentry,
        Fishing,
        Farming,
        Breeding,
        Market,
        Home,
        Demonstration,
        Base



    }


    public enum e_TypeNode
    {
        Mineral,
        Logs,
        Fish,
        Crops,
        Mob
    }


    public enum e_TypeRewardAds
    {
        Energy = 0,
        Rupy = 1,
        Luck,
        Gacha,
        Progression,
        Progressive
    }

    public enum e_SeceneIndex
    {
        Register,
        Home,
        Mining,
        Carpentry,
        Farming,
        Market,
        Breeding,
        UI
    }

}
