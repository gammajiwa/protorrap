using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Drops : MonoBehaviour
{
    Rigidbody2D m_Rb;
    public float m_SideForce;
    public float m_SideMove;
    public Vector2 m_UpForce;

    float m_Timer;

    Transform m_Player;
    // public float m_UPForce;
    void Start()
    {
        f_Init();
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        m_Timer -= 1 * Time.fixedDeltaTime;
        if (m_Timer <= 0)
        {
            Vector3 direction = (m_Player.transform.position - transform.position).normalized;
            if (Vector3.Distance(gameObject.transform.position, m_Player.transform.position) >= 1)
            {
                m_Rb.MovePosition(transform.position + direction * m_SideMove * Time.fixedDeltaTime);
            }
        }
    }

    public void f_Init()
    {
        float t_xForce = Random.Range(-m_SideForce, m_SideForce);
        float t_yForce = Random.Range(m_UpForce.x, m_UpForce.y);

        Vector3 t_Force = new Vector3(t_xForce, t_yForce, 0);
        m_Rb = GetComponent<Rigidbody2D>();
        m_Rb.velocity = t_Force;
        m_Timer = 1;

    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            f_DestroyItem();
    }


    public void f_DestroyItem()
    {
        Destroy(gameObject);
    }

}
