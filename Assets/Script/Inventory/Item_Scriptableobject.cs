using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumerator;
[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item_ : ScriptableObject
{
    public string m_NameItem;
    public string m_Des;
    public Sprite m_Sprite;
    public e_TypeNode m_Type;
    public int m_Size;
    public int m_Price;
    public bool m_FirstTime;
}
