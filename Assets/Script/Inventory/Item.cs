using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Item : MonoBehaviour
{
    LootDatabase_Scriptable m_Item;
    public Image m_ItemImage;
    public TextMeshProUGUI m_SizeText;
    public int m_IndexItemInventory;

    public int m_Size;

    public void f_ItemDisplay(LootDatabase_Scriptable p_Item)
    {
        m_Item = p_Item;

        for (int i = 0; i < Inventory.m_Instance.m_ItemsInInventory.Count; i++)
        {
            if (Inventory.m_Instance.m_ItemsInInventory[i].m_LootName == p_Item.m_LootName)
            {
                m_IndexItemInventory = i;
            }
        }
        m_ItemImage.sprite = p_Item.m_LootImage;
        m_Size = p_Item.m_Size;
        m_SizeText.text = m_Size.ToString();
        if (m_Size < 1)
        {
            f_ClearDisplay();
        }
    }
    public void f_ClearDisplay()
    {
        m_Item = null;
        m_ItemImage.sprite = null;
        m_Size = 0;
        m_SizeText.text = "";
    }



    public void f_PickItemSell()
    {
        Market.m_Instance.m_ItemPick = true;
        Market.m_Instance.f_DetailItem(m_Item);
    }


    public void f_ItemInfo()
    {
        Item_Slot.m_Instance.f_ItemInfo(m_Item);
    }


}
