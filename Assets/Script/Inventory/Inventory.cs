using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Inventory : MonoBehaviour
{
    public static Inventory m_Instance;
    public List<LootDatabase_Scriptable> m_Items;
    public List<LootDatabase_Scriptable> m_QuestFoodex = new List<LootDatabase_Scriptable>();
    public List<LootDatabase_Scriptable> m_ItemsInInventory = new List<LootDatabase_Scriptable>();
    public List<LootDatabase_Scriptable> m_FoodPedia = new List<LootDatabase_Scriptable>();
    public List<LootDatabase_Scriptable> m_ItemsShort = new List<LootDatabase_Scriptable>();
    public int[] m_SizeIte;
    public delegate void FoodPediaUpdate();
    public FoodPediaUpdate FoodPediaUpdateCallBack;

    private void Awake()
    {
        m_Instance = this;
    }

    private void Start()
    {
        foreach (LootDatabase_Scriptable item in m_Items)
        {
            item.m_Size = 0;
        }
    }


    public void f_AddInventory(string p_name)
    {
        foreach (LootDatabase_Scriptable item in m_Items)
        {
            if (item.m_LootName == p_name)
            {
                m_ItemsInInventory.Add(item);
                item.m_Size++;
                for (int i = 0; i < m_QuestFoodex.Count; i++)
                {
                    if (m_QuestFoodex[i].m_LootName == item.m_LootName)
                    {
                        m_QuestFoodex.RemoveAt(i);
                        m_FoodPedia.Add(item);
                        StopAllCoroutines();
                        StartCoroutine(f_PoopUp(p_name));
                        FoodPediaUpdateCallBack?.Invoke();
                    }
                }
            }
            else
            {
                item.m_Size++;
            }

            // for (int i = 0; i < m_ItemsInInventory.Count; i++)
            // {
            //     if (m_ItemsInInventory[i].m_LootName == p_name)
            //     {
            //         m_SizeItem[i]++;
            //         return;
            //     }
            // }
        }
        // for (int i = 0; i < m_Items.Count; i++)
        // {
        //     m_ItemsInInventory.Add(m_Items[i]);
        //     m_SizeItem.Add(1);
        //     for (int j = 0; j < m_QuestFoodex.Count; j++)
        //     {
        //         if (m_QuestFoodex[j].m_LootName == m_Items[i].m_LootName)
        //         {
        //             m_QuestFoodex.RemoveAt(j);
        //             m_FoodPedia.Add(m_Items[i]);
        //             StopAllCoroutines();
        //             StartCoroutine(f_PoopUp(p_name));
        //             FoodPediaUpdateCallBack?.Invoke();
        //         }
        //     }
        // }
    }



    IEnumerator f_PoopUp(string p_name)
    {
        yield return new WaitForSeconds(1);
        Game_Manager.m_Instance.f_PopUpFoodex(p_name);
    }

    public void f_CheckItem()
    {
        for (int i = 0; i < m_ItemsInInventory.Count; i++)
        {
            if (m_ItemsInInventory[i].m_Size < 1)
            {
                m_ItemsInInventory.RemoveAt(i);
            }
        }
    }
}
