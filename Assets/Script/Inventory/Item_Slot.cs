using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Item_Slot : MonoBehaviour
{
    public static Item_Slot m_Instance;
    public TextMeshProUGUI m_IDName;
    public Image m_ProfileImage;
    public TextMeshProUGUI m_Des;
    public TextMeshProUGUI m_Price;





    public Transform m_Parent;

    public Item[] m_Items;

    public Item m_Select;

    private void Awake()
    {
        m_Instance = this;
    }
    void Start()
    {
        m_Items = m_Parent.GetComponentsInChildren<Item>();
    }


    public void f_UpdateUiInventory(List<LootDatabase_Scriptable> p_Item)
    {
        for (int i = 0; i < m_Items.Length; i++)
        {
            if (i < p_Item.Count)
            {
                m_Items[i].f_ItemDisplay(p_Item[i]);
            }
            else
            {
                m_Items[i].f_ClearDisplay();
            }
        }
    }



    public void f_ItemsShort(int p_Type)
    {

        Inventory.m_Instance.m_ItemsShort.Clear();

        foreach (LootDatabase_Scriptable item in Inventory.m_Instance.m_ItemsInInventory)
        {
            if ((int)item.m_Type == p_Type)
            {
                Inventory.m_Instance.m_ItemsShort.Add(item);
            }
            f_UpdateUiInventory(Inventory.m_Instance.m_ItemsShort);
        }
    }


    public void f_ItemInfo(LootDatabase_Scriptable p_Item)
    {
        m_ProfileImage.sprite = p_Item.m_LootImage;
        m_Des.text = p_Item.m_LootDes;
        m_Price.text = "$ " + p_Item.m_Price.ToString();
        m_IDName.text = p_Item.m_LootName;
    }

    public void f_ClerInfo()
    {
        m_ProfileImage.sprite = null;
        m_Des.text = "";
        m_Price.text = "";
        m_IDName.text = "";
    }


}
