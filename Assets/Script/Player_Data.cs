using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumerator;

public class Player_Data : MonoBehaviour
{

    public static Player_Data m_Instance;
    [Header("Player Data")]
    public int m_AgePlayer;
    public float m_MaxEnergy;
    public float m_CurrEnergy;
    public float m_RegenEnergy;
    public int m_RupyCurrency;
    public int m_RecehCurrency;
    public int m_FarmLots;
    public List<int> m_UnlockFarm = new List<int>();



    [Header("Ads")]
    public int m_CloverAds;
    public int m_ProgressiveAds;
    public int m_LuckIndex;
    public float m_TimerAds5Minute;
    public float m_TimerLuck;
    public e_TypeRewardAds m_TypeRewardAds;
    public bool m_IsTripleLuck;


    [Header("Player Conditions")]
    public bool m_IsSleep;
    public bool m_FirstLogin;





    private void Awake()
    {
        m_Instance = this;
    }


    private void Update()
    {
        f_TimerCloverAds();
        f_Ads5Minute();
    }




    public void f_Ads5Minute()
    {
        if (!m_FirstLogin)
        {
            m_TimerAds5Minute -= 1 * Time.deltaTime;
            if (m_TimerAds5Minute <= 0)
            {
                AdScript.m_Instance.f_AddAds(Enumerator.e_TypeRewardAds.Rupy);

            }
        }
    }


    void f_TimerCloverAds()
    {
        if (m_TimerLuck >= 0)
            m_TimerLuck -= 1 * Time.deltaTime;
        else f_DefaultLuck();



    }

    void f_DefaultLuck()
    {
        m_LuckIndex = (m_IsTripleLuck) ? 3 : 1;
    }




}
