using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ManageMarket : MonoBehaviour
{

    public GameObject[] m_ActivityManager;
    public void f_RupyStore()
    {
        UI_Manager.m_Instance.f_OpenTabGame(7);
    }

    public void f_MarketStore(int p_Index)
    {
        Market.m_Instance.f_ItemsShort(p_Index);
        Market.m_Instance.f_SellItem();
        UI_Manager.m_Instance.f_OpenTabGame(8);
        Market.m_Instance.m_IDMarket = p_Index;
        Inventory.m_Instance.f_CheckItem();
        F_MarketActivity_Manager(p_Index);
        Market.m_Instance.f_TittleMarket(p_Index);
    }



    void F_MarketActivity_Manager(int p_Index)
    {
        foreach (var item in m_ActivityManager)
        {
            item.SetActive(false);
        }
        m_ActivityManager[p_Index].SetActive(true);
    }
}
