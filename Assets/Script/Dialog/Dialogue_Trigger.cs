using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Dialogue_Trigger : MonoBehaviour
{

    public static Dialogue_Trigger m_Instance;
    public Dialogue[] m_Dialogues;
    public Dialogue m_DialogueActive;



    private void Awake()
    {
        m_Instance = this;
    }
    private void Start()
    {


    }


    public void f_DialogueTrigger(string p_Name)
    {
        f_DialogueActive(p_Name);
        Dialogue_Manager.m_Instance.f_StartDialogue(m_DialogueActive);

    }

    void f_DialogueActive(string p_Name)
    {
        m_DialogueActive = Array.Find(m_Dialogues, Dialogue => Dialogue.m_Name == p_Name);

    }



}
