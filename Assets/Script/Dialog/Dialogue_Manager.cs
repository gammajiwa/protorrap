using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Dialogue_Manager : MonoBehaviour
{
    // Start is called before the first frame update
    public static Dialogue_Manager m_Instance;
    private Queue<string> m_Sentence;
    [Header("Dialogue")]
    public TextMeshProUGUI m_NameText;
    public TextMeshProUGUI m_DialogueText;
    int t_DialogueIndex;

    private void Awake()
    {
        m_Instance = this;
        m_Sentence = new Queue<string>();
    }



    public void f_StartDialogue(Dialogue p_Dialogue)
    {
        if (Game_Manager.m_Instance.m_GameActivity != Enumerator.e_Activities.Register)
        {
            UI_Manager.m_Instance.m_NameText.text = p_Dialogue.m_Name;
            m_Sentence.Clear();
            foreach (string Sentence in p_Dialogue.m_Sentence)
            {
                m_Sentence.Enqueue(Sentence);
            }
            f_DisplayNextSentance();
        }
    }
    public void f_DisplayNextSentance()
    {
        if (m_Sentence.Count == 0)
        {
            f_EndDialogue();
            return;
        }
        if (Dialogue_Trigger.m_Instance.m_DialogueActive.m_Name != "Tanuki")
        {
            StopAllCoroutines();
            string _Sentance = m_Sentence.Dequeue();
            StartCoroutine(f_TypeSentance(_Sentance));
        }
        else
        {
            if (Tutorial_Manager.m_Instance.m_Tutorial)
            {
                Game_Manager.m_Instance.m_IndexTutorial++;
                Tutorial_Manager.m_Instance.f_ChangeScene();
            }
            string _Sentance = m_Sentence.ToArray()[Game_Manager.m_Instance.m_IndexTutorial];
            StopAllCoroutines();
            StartCoroutine(f_TypeSentance(_Sentance));
        }

    }

    void f_EndDialogue()
    {
        Debug.Log("End of Conversations");
        if (Tutorial_Manager.m_Instance.m_Tutorial)
        {
            Dialogue_Trigger.m_Instance.f_DialogueTrigger("Tanuki");
        }
    }



    IEnumerator f_TypeSentance(string p_Sentance)
    {
        m_DialogueText.text = "";

        foreach (char _Text in p_Sentance.ToCharArray())
        {
            m_DialogueText.text += _Text;
            yield return null;
        }

    }


    public void f_IndexDialogue(int p_Index)
    {
        t_DialogueIndex = p_Index;

    }
}
