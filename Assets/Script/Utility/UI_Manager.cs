using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UI_Manager : MonoBehaviour
{
    public static UI_Manager m_Instance;
    Player_Data t_Playerdata;
    [TextArea(3, 8)]

    public string[] m_DesTextAds;
    public Image m_Energy;
    public Image[] m_AdsCloverGameObject;


    [Header("UI Game")]
    public TextMeshProUGUI m_DesAds;
    public TextMeshProUGUI m_LuckyText;
    public TextMeshProUGUI m_RupyCurrency;
    public TextMeshProUGUI m_RecehCurrency;
    public TextMeshProUGUI m_Locations;
    [Header("Dialogue")]
    public TextMeshProUGUI m_NameText;
    public TextMeshProUGUI m_DialogueText;


    [Header("Quest")]
    public TextMeshProUGUI m_DesQuestText;
    public TextMeshProUGUI m_ProgressQuestText;
    public GameObject m_QuestGameObject;


    [Header("GameObject")]

    public GameObject m_LoadingScreen;
    public GameObject m_Map;
    public GameObject m_HideButton;
    public GameObject m_SettingGame;
    public GameObject m_AccountTab;
    public GameObject m_Inventory;
    public GameObject m_IAPShop;
    public GameObject m_FoodexUI;
    public GameObject m_FoodPedia;
    public GameObject m_ItemDrop;
    public GameObject m_AdsReward;
    public GameObject m_RupyShop;
    public GameObject m_UIMaster;
    public GameObject m_Store;
    public GameObject m_MoneyBar;
    public Transform m_PosPopUp;


    public Slider m_LoadSlide;



    [Header("Game Seting")]
    public RectTransform m_ToggelGameObject;
    public Toggle m_ToggelNotif;
    public Color m_DefaultColor;


    private void Awake()
    {
        m_Instance = this;
    }
    void Start()
    {
        t_Playerdata = Player_Data.m_Instance;
        f_UpdateCurrency();
        f_ActiveUI();

    }

    void Update()
    {
        m_LuckyText.text = "Luck = " + t_Playerdata.m_LuckIndex.ToString();
        m_Energy.fillAmount = t_Playerdata.m_CurrEnergy / t_Playerdata.m_MaxEnergy;
        if (t_Playerdata.m_IsSleep && t_Playerdata.m_CurrEnergy <= t_Playerdata.m_MaxEnergy)
        {
            f_RefillEnergy();
        }
    }



    void f_RefillEnergy()
    {
        t_Playerdata.m_CurrEnergy += t_Playerdata.m_RegenEnergy * Time.deltaTime;
        if (Player_Data.m_Instance.m_FirstLogin == true && t_Playerdata.m_CurrEnergy >= t_Playerdata.m_MaxEnergy)
        {
            Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Energy, 1);
            Tutorial_Manager.m_Instance.f_ContinueTutorial();
            t_Playerdata.m_IsSleep = false;
        }
    }


    public void f_UpdateCurrency()
    {
        m_RupyCurrency.text = "$" + Player_Data.m_Instance.m_RupyCurrency.ToString();
        m_RecehCurrency.text = "$" + Player_Data.m_Instance.m_RecehCurrency.ToString();
    }

    public void f_DisplayCloversAds()
    {
        if (!Player_Data.m_Instance.m_FirstLogin)
        {
            foreach (var item in m_AdsCloverGameObject)
            {
                item.color = new Color32(0, 0, 0, 0);
                for (int i = 0; i < Player_Data.m_Instance.m_CloverAds; i++)
                {
                    if (Player_Data.m_Instance.m_CloverAds > 0)
                    {
                        m_AdsCloverGameObject[i].color = new Color32(255, 255, 255, 255);
                    }
                }
            }
        }
    }




    public void f_HideButton()
    {
        if (!Player_Data.m_Instance.m_FirstLogin)
            m_HideButton.SetActive(!m_HideButton.activeSelf);
    }

    public void f_OpenInventory()
    {
        if (!Player_Data.m_Instance.m_FirstLogin)
        {
            m_Inventory.SetActive(!m_Inventory.activeSelf);
            Item_Slot.m_Instance.f_UpdateUiInventory(Inventory.m_Instance.m_ItemsInInventory);
            // Inventory.m_Instance.f_CheckItem();
            Item_Slot.m_Instance.f_ClerInfo();

        }

    }

    public void f_OpenTabGame(int p_Index)
    {
        Game_Manager.m_Instance.f_OpenTabGame(p_Index);
    }



    public void f_Map(int p_index)
    {
        if (!Player_Data.m_Instance.m_FirstLogin)
            switch (p_index)
            {
                case 1:
                    Game_Manager.m_Instance.f_Load("Mining", p_index);
                    break;
                case 2:
                    Game_Manager.m_Instance.f_Load("Carpentry", p_index);
                    break;
                case 3:
                    Game_Manager.m_Instance.f_Load("Fishing", p_index);
                    break;
                case 4:
                    Game_Manager.m_Instance.f_Load("Farming", p_index);
                    break;
                case 5:
                    Game_Manager.m_Instance.f_Load("Breeding", p_index);
                    break;
                case 6:
                    Game_Manager.m_Instance.f_Load("Market", p_index);
                    break;
                case 7:
                    Game_Manager.m_Instance.f_Load("Home", p_index);
                    break;
            }
        UI_Manager.m_Instance.m_Map.SetActive(false);
    }


    public void f_ActiveUI()
    {
        if (Game_Manager.m_Instance.m_GameActivity == Enumerator.e_Activities.Register)
        {
            m_UIMaster.SetActive(false);
        }
        else m_UIMaster.SetActive(true);
    }


    public void f_ShortItemUI(int p_Type)
    {
        Item_Slot.m_Instance.f_ItemsShort(p_Type);
    }
    public void f_AllItemUI()
    {
        Item_Slot.m_Instance.f_UpdateUiInventory(Inventory.m_Instance.m_ItemsInInventory);
    }
}
