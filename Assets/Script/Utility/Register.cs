using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Register : MonoBehaviour
{
    public Toggle m_AcceptPrivacyPolicy;
    public GameObject m_Notice;
    public GameObject m_Register;
    private void Awake()
    {

    }

    public void f_Register()
    {
        // if (m_AcceptPrivacyPolicy.isOn == true)
        // {
        m_Register.SetActive(false);
        Game_Manager.m_Instance.f_Load("Home", 7);
        UI_Manager.m_Instance.f_ActiveUI();
        if (Player_Data.m_Instance.m_FirstLogin)
        {
            Tutorial_Manager.m_Instance.m_Tutorial = true;
            Dialogue_Trigger.m_Instance.f_DialogueTrigger("Tanuki");
        }
        // }
        // else
        //     m_Notice.SetActive(true);

    }
}
