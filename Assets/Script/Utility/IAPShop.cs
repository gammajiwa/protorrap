using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPShop : MonoBehaviour
{
    string m_RupyCurrencyPacks = "com.gammajiwa.protorrap.com5500";
    string m_FullEnergyBottle = "com.gammajiwa.protorrap.comFullEnergyBottle";
    string m_SupporterPack = "com.gammajiwa.protorrap.comSupporterPack";
    string m_RestPack = "com.gammajiwa.protorrap.comNoRestPack";
    string m_TripleLuck = "com.gammajiwa.protorrap.comPermanentTripleLuck";

    public void f_OnPurchaseComplete(Product p_Product)
    {
        if (p_Product.definition.id == m_RupyCurrencyPacks)
        {
            Debug.Log("Player Beli Rupy 5500");
            Player_Data.m_Instance.m_RupyCurrency += 5500;
            UI_Manager.m_Instance.f_UpdateCurrency();
        }
        if (p_Product.definition.id == m_FullEnergyBottle)
        {
            Debug.Log("Full Energy Bottle");
            Player_Data.m_Instance.m_CurrEnergy = Player_Data.m_Instance.m_MaxEnergy;
        }
        if (p_Product.definition.id == m_SupporterPack)
        {
            Debug.Log("Special William Appearance + Rupy 1500 + 50% Ener gy Bottle");
            Player_Data.m_Instance.m_CurrEnergy = Player_Data.m_Instance.m_MaxEnergy / 2;
            Player_Data.m_Instance.m_RupyCurrency += 1500;
            UI_Manager.m_Instance.f_UpdateCurrency();
        }
        if (p_Product.definition.id == m_RestPack)
        {
            Debug.Log("Full Energy Bottle + Quadruple Luck 30 Mins + Rupy 2500");
            Player_Data.m_Instance.m_CurrEnergy = Player_Data.m_Instance.m_MaxEnergy;
            Player_Data.m_Instance.m_RupyCurrency += 2500;
            UI_Manager.m_Instance.f_UpdateCurrency();
            Player_Data.m_Instance.m_LuckIndex = 4;
            Player_Data.m_Instance.m_TimerLuck = 1800;
        }
        if (p_Product.definition.id == m_TripleLuck)
        {
            Debug.Log("Permanent Triple Luck");
            Player_Data.m_Instance.m_IsTripleLuck = true;
            if (Player_Data.m_Instance.m_LuckIndex == 1)
                Player_Data.m_Instance.m_LuckIndex = 3;
            else Player_Data.m_Instance.m_LuckIndex = 4;

        }
    }
    public void f_OnPurchaseFailed(Product p_Product, PurchaseFailureReason p_Reason)
    {
        Debug.Log("Player gagal Beli " + p_Product.definition.id + " " + p_Reason);
    }
}
