using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_Setting : MonoBehaviour
{

    Vector2 m_PosToggel;



    private void Awake()
    {
        // if (Game_Manager.m_Instance.m_GameActivity != Enumerator.e_Activities.Register)
        // {
        //     m_PosToggel = UI_Manager.m_Instance.m_ToggelGameObject.anchoredPosition;
        //     UI_Manager.m_Instance.m_ToggelNotif.onValueChanged.AddListener(f_OnSwitch);
        // }
    }

    void f_OnSwitch(bool p_On)
    {
        UI_Manager.m_Instance.m_ToggelGameObject.anchoredPosition = p_On ? m_PosToggel * -1 : m_PosToggel;
        UI_Manager.m_Instance.m_ToggelGameObject.GetComponent<Image>().color = p_On ? Color.green : UI_Manager.m_Instance.m_DefaultColor;
    }

    // private void OnDestroy()
    // {
    //     UI_Manager.m_Instance.m_ToggelNotif.onValueChanged.RemoveListener(f_OnSwitch);

    // }
}
