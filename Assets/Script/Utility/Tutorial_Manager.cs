using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Tutorial_Manager : MonoBehaviour
{
    public static Tutorial_Manager m_Instance;

    public bool m_Tutorial;
    public GameObject m_TutorialGameObject;


    private void Awake()
    {
        m_Instance = this;

    }
    void Start()
    {
        if (Game_Manager.m_Instance.m_IndexTutorial == 32)
        {
            Player_Data.m_Instance.m_CurrEnergy = 0;
        }


    }
    private void Update()
    {
        if (Game_Manager.m_Instance.m_GameActivity != Enumerator.e_Activities.Register)
        {
            if (m_Tutorial)
            {
                m_TutorialGameObject.SetActive(true);
                Player_Data.m_Instance.m_RegenEnergy = 30f;
            }
            if (!m_Tutorial)
            {
                m_TutorialGameObject.SetActive(false);
            }
        }


    }



    public void f_ChangeScene()
    {

        PlayerPrefs.SetInt("IndexTutorial", Game_Manager.m_Instance.m_IndexTutorial);

        switch (Game_Manager.m_Instance.m_IndexTutorial)
        {
            case 3:
                Debug.Log("jalan");
                f_CheckScene("Mining", 1);
                break;
            case 4:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tmineral");
                break;
            case 5:
                f_CheckScene("Carpentry", 2);
                break;
            case 6:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tlog");
                break;
            case 7:
                f_CheckScene("Fishing", 3);
                break;
            case 9:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tfish");
                break;
            case 10:
                f_CheckScene("Farming", 4);
                break;
            case 11:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tfarm");
                break;
            case 12:
                f_CheckScene("Market", 6);
                break;
            case 14:
                Dialogue_Trigger.m_Instance.f_DialogueTrigger("NPC A");
                // m_Tutorial = false;
                // Quest_Manager.m_Instance.f_ActiveQuest("Market");
                break;
            case 16:
                Dialogue_Trigger.m_Instance.f_DialogueTrigger("NPC B");
                break;
            case 18:
                Dialogue_Trigger.m_Instance.f_DialogueTrigger("NPC C");
                break;
            case 20:
                Dialogue_Trigger.m_Instance.f_DialogueTrigger("NPC D");
                break;
            case 22:
                Dialogue_Trigger.m_Instance.f_DialogueTrigger("NPC E");
                break;
            case 24:
                Dialogue_Trigger.m_Instance.f_DialogueTrigger("NPC F");
                break;
            case 26:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tsell");
                break;
            case 27:
                UI_Manager.m_Instance.f_OpenTabGame(8);
                f_CheckScene("Breeding", 5);
                break;
            case 31:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tmob");
                break;
            case 32:
                f_CheckScene("Home", 7);
                break;
            case 35:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Tenergy");
                break;
            case 37:
                m_Tutorial = false;
                Quest_Manager.m_Instance.f_ActiveQuest("Thome");
                Player_Data.m_Instance.m_FirstLogin = false;
                break;
        }

    }

    void f_CheckScene(string p_Scene, int p_)
    {
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName(p_Scene))
        {
            Game_Manager.m_Instance.f_Load(p_Scene, p_);
        }
    }

    public void f_ContinueTutorial()
    {
        if (Player_Data.m_Instance.m_FirstLogin)
        {
            // Dialogue_Trigger.m_Instance.f_DialogueTrigger();
            m_Tutorial = true;
        }
        else
            m_Tutorial = false;
    }
}

