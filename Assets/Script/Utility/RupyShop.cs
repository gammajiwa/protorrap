using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class RupyShop : MonoBehaviour
{
    public TextMeshProUGUI m_TittleText;
    public TextMeshProUGUI m_DesText;
    public TextMeshProUGUI m_PriceText;

    public Button m_Btn;

    public string m_DesString;
    public string m_TittleString;
    public int m_Price;

    // Update is called once per frame
    void Update()
    {
        m_DesText.text = m_DesString;
        m_TittleText.text = m_TittleString;
        m_PriceText.text = "$ " + m_Price.ToString();

        if (Player_Data.m_Instance.m_RupyCurrency >= m_Price)
            m_Btn.interactable = true;
        else m_Btn.interactable = false;

    }


    public void f_Receh()
    {
        Player_Data.m_Instance.m_RupyCurrency -= m_Price;
        Player_Data.m_Instance.m_RecehCurrency += 10000;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Debug.Log("Dafat Receh");
    }
    public void f_EnergyBottle()
    {
        Player_Data.m_Instance.m_RupyCurrency -= m_Price;
        Player_Data.m_Instance.m_CurrEnergy += Player_Data.m_Instance.m_MaxEnergy / 2;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Debug.Log("Dafat 50% Energy Bottle");
    }
    public void f_CraftingMaterialChest()
    {
        Player_Data.m_Instance.m_RupyCurrency -= m_Price;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Debug.Log("Dafat Crafting Material Chest");
    }
    public void f_LegendaryCraftingMaterialChest()
    {
        Player_Data.m_Instance.m_RupyCurrency -= m_Price;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Debug.Log("Dafat Legendary Crafting Material Chest");
    }

    public void f_TripleLuckBottle30Mins()
    {
        Player_Data.m_Instance.m_RupyCurrency -= m_Price;
        Player_Data.m_Instance.m_LuckIndex = 3;
        Player_Data.m_Instance.m_TimerLuck = 1800;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Debug.Log("Dafat Triple Luck Bottle 30 Mins");
    }
    public void f_QuadrupleLuckBottle30Mins()
    {
        Player_Data.m_Instance.m_RupyCurrency -= m_Price;
        Player_Data.m_Instance.m_LuckIndex = 4;
        Player_Data.m_Instance.m_TimerLuck = 1800;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Debug.Log("Dafat Quadruple Luck Bottle 30 Mins");
    }
}

