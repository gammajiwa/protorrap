using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ID_Crops : MonoBehaviour
{
    public int m_ID;
    public int m_IdParentCrops;
    public bool m_Ripe;

    public Image m_Color;

    float aa;
    public int m_Distance;
    public Transform m_Anchor;

    Crops_GameObject m_Crops;

    private void Start()
    {
        m_Crops = GetComponentInParent<Crops_GameObject>();
        m_IdParentCrops = m_Crops.m_IdCrops;
        Selection_Crops.m_Instance.m_FindeAchor += f_FindeAnchor;

    }

    private void Update()
    {
        if (m_Color.color == Color.red)
            m_Ripe = true;

    }




    public void f_BtnCropsSlot()
    {
        if (m_Crops.m_SizePlant == 0)
        {
            Crops_Manager.m_Instance.m_IndexIdCrops = m_IdParentCrops;
            Crops_Manager.m_Instance.OnCropsClickCallBack?.Invoke();

        }
        else if (m_Color.color != Color.white)
        {
            m_Ripe = false;
            foreach (var item in Selection_Crops.m_Instance.m_Crops)
            {
                item.tag = "crop";
            }
            gameObject.tag = "anchor";
            f_SelectionCrop(m_ID);
            Selection_Crops.m_Instance.f_OnClickSelection();
            // Selection_Crops.m_Instance.f_RemoveSelection(m_ID);
        }
    }


    void f_FindeAnchor()
    {
        m_Anchor = GameObject.FindGameObjectWithTag("anchor").GetComponent<Transform>();
        f_CheckDistance();
    }

    public void f_SelectionCrop(int p_Index)
    {

        m_Crops.f_Harvest(p_Index);
    }

    void f_CheckDistance()
    {
        aa = Vector3.Distance(transform.position, m_Anchor.transform.position);
        m_Distance = Mathf.RoundToInt(aa);

    }


}
