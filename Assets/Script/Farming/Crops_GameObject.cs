using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Enum;

public class Crops_GameObject : MonoBehaviour
{
    public int m_IdCrops;
    public Image[] m_CropsSlot;
    public Image m_BarTimer;

    public GameObject m_Time;
    public GameObject m_LockedGameObject;

    public float m_Timer;
    public float m_MaxTimer;
    public int m_SizePlant;
    public int m_IdPlant;


    public bool m_Locked = true;
    public bool m_Planted;
    public bool m_Ripe;
    int m_IdHarvest;



    private void Awake()
    {
        f_Init();
    }

    private void Start()
    {
        if (m_Locked)
            m_LockedGameObject.SetActive(true);
    }

    void Update()
    {
        m_BarTimer.fillAmount = m_Timer / m_MaxTimer;

        if (m_Planted)
        {
            if (m_Timer < m_MaxTimer)
                m_Timer += 1 * Time.deltaTime;
            else
            {
                f_Ripeplant();

            }
            // m_Time = (int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.INSTAGROW);
        }



    }
    void f_Init()
    {
        m_SizePlant = 0;
        m_Timer = 0;
        m_Ripe = false;
        m_Planted = false;
        // foreach (var item in m_CropsSlot)
        // {
        //     item.tag = "crop";
        // }
    }


    public void f_Plant(int p_Index, Color p_Color)
    {

        foreach (Image m_Crops in m_CropsSlot)
        {
            m_Crops.color = p_Color;
            m_Crops.GetComponent<Button>().interactable = false;
        }
        m_IdPlant = p_Index;
        m_Planted = true;
        m_Time.SetActive(true);


    }


    void f_Ripeplant()
    {
        foreach (Image m_Crops in m_CropsSlot)
        {
            m_Crops.color = Color.red;
            m_Crops.GetComponent<Button>().interactable = true;
        }
        m_SizePlant = 9;
        m_Ripe = true;
        m_Planted = false;
        m_Time.SetActive(false);
        Selection_Crops.m_Instance.f_SelectionCrops();

    }



    // public void f_BtnCropsSlot()
    // {
    //     if (m_SizePlant == 0)
    //     {
    //         Crops_Manager.m_Instance.m_IndexIdCrops = m_IdCrops;
    //         Crops_Manager.m_Instance.OnCropsClickCallBack?.Invoke();

    //     }
    //     else
    //         f_Harvest();
    // }

    public void f_Harvest(int p_Index)
    {

        // foreach (Image m_Crops in m_CropsSlot)
        // {
        //     // m_Crops.color = Color.white;
        //     // Debug.Log("Panen Tanaman index ke" + m_IdPlant);
        //     m_IdHarvest = m_Crops.GetComponent<ID_Crops>().m_ID;
        // }
        f_HarvestCrops(p_Index);
        if (m_SizePlant == 0)
            f_Init();

    }

    public void f_UnlockCrops()
    {
        if (Tutorial_Manager.m_Instance.m_Tutorial == false)
            Crops_Manager.m_Instance.OnUnLockCropsCallBack?.Invoke(m_IdCrops);
    }

    void f_HarvestCrops(int p_Index)
    {
        Crops_Manager.m_Instance.m_SizeRadius = (int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.HARVESTSAUCE);
        m_CropsSlot[p_Index].color = Color.white;
        Debug.Log("Panen Tanaman Zenis ke" + m_IdPlant);
        m_SizePlant--;


        //tutorial & Quest
        // Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Crops, 1);
        // Inventory.m_Instance.f_AddInventory("Crops");
        Game_Manager.m_Instance.f_ItemDrops(gameObject.transform.position, "Crops", c_QuestGoal.e_Goaltype.Crops, 1);
        // Activity_Manager.m_Instance.f_Action();
        // if (Quest_Manager.m_Instance.m_QuestActive.m_IsActive)
        // {
        //     Quest_Manager.m_Instance.m_QuestActive.m_Goal.f_QuestProgress(c_QuestGoal.e_Goaltype.Crops);
        //     if (Quest_Manager.m_Instance.m_QuestActive.m_Goal.f_IsReached())
        //     {
        //         Tutorial_Manager.m_Instance.f_ContinueTutorial();
        //         Quest_Manager.m_Instance.m_QuestActive.f_Complete();
        //         Quest_Manager.m_Instance.m_QuestGameObject.SetActive(false);
        //     }
        // }

    }


}
