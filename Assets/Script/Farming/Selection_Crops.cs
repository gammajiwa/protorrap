using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Selection_Crops : MonoBehaviour
{

    public static Selection_Crops m_Instance;
    public List<GameObject> m_Crops = new List<GameObject>();
    public List<GameObject> m_CropsRipe = new List<GameObject>();
    public event Action m_FindeAchor;





    private void Awake()
    {
        m_Instance = this;

    }


    public void f_SelectionCrops()
    {
        m_Crops.Clear();
        m_Crops.AddRange(GameObject.FindGameObjectsWithTag("crop"));

    }

    private void Update()
    {

    }

    public void f_OnClickSelection()
    {
        m_FindeAchor?.Invoke();

        foreach (GameObject item in m_Crops)
        {

            if (item.GetComponent<ID_Crops>().m_Ripe == true)
            {
                m_CropsRipe.Add(item);
            }
        }

        // foreach (var item in m_CropsRipe)
        // {
        //     m_NearCrops.Add(item.GetComponent<ID_Crops>().m_Distance);
        //     // f_SelectionSort(m_NearCrops);
        //     m_NearCrops.Sort();
        // }
        if (m_CropsRipe.Count > 0)
        {
            m_CropsRipe.Sort(delegate (GameObject a, GameObject b)
           {
               return (a.GetComponent<ID_Crops>().m_Distance).CompareTo(b.GetComponent<ID_Crops>().m_Distance);
           }
           );
        }
        f_Selection();
    }

    void f_Selection()
    {
        for (int i = 0; i < Crops_Manager.m_Instance.m_SizeRadius; i++)
        {

            ID_Crops Crop = m_CropsRipe[i].GetComponent<ID_Crops>();

            Crop.f_SelectionCrop(Crop.m_ID);
            Crop.m_Ripe = false;
            // m_CropsRipe.Remove(m_CropsRipe[i]);
        }

        m_CropsRipe.Clear();
    }

    // public void f_RemoveSelection(int p_Index)
    // {
    //     for (int i = 0; i < m_CropsRipe.Count; i++)
    //     {
    //         if (m_CropsRipe[i].GetComponent<ID_Crops>().m_ID == p_Index)
    //         {
    //             m_CropsRipe.Remove(m_CropsRipe[i]);
    //         }
    //     }
    // }






    // void f_SelectionSort(List<int> p_Crops)
    // {
    //     int m_Min;
    //     int m_Temp;

    //     for (int i = 0; i < p_Crops.Count; i++)
    //     {
    //         m_Min = i;
    //         for (int j = 0; j < p_Crops.Count; j++)
    //         {
    //             if (p_Crops[j] < p_Crops[m_Min])
    //             {
    //                 m_Min = j;
    //             }
    //         }
    //         if (m_Min != i)
    //         {
    //             m_Temp = p_Crops[i];
    //             p_Crops[i] = p_Crops[m_Min];
    //             p_Crops[m_Min] = m_Temp;
    //         }
    //     }
    // }



}



