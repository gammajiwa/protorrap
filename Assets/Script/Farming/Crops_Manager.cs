using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum;
public class Crops_Manager : MonoBehaviour
{
    public static Crops_Manager m_Instance;
    public Crops_GameObject[] Crops_GameObjec;
    public GameObject m_Seedbar;
    public GameObject m_ConfirmationCrops;
    public int m_IndexIdCrops;
    public int m_IndexUnlock;
    public int m_SizeRadius;

    public GameObject m_Btnsskill;

    public delegate void OnCropsClick();
    public OnCropsClick OnCropsClickCallBack;
    public delegate void OnSeedClick(int p_Seed, Color p_Color);
    public OnSeedClick OnSeedClickCallBack;
    public delegate void OnUnLockCrops(int p_Index);
    public OnUnLockCrops OnUnLockCropsCallBack;

    private void Awake()
    {
        m_Instance = this;
    }
    void Start()
    {
        OnCropsClickCallBack += f_SeedBarActived;
        OnSeedClickCallBack += f_OnSeedClick;
        OnUnLockCropsCallBack += f_UnLockCrops;
    }

    private void Update()
    {
        m_SizeRadius = (int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.HARVEST);
        f_BtnSkill(Farming_Manager.m_Instance.m_Mastery.m_Level);
    }


    void f_SeedBarActived()
    {
        m_Seedbar.SetActive(true);
    }

    void f_OnSeedClick(int p_Seed, Color p_Color)
    {
        Crops_GameObjec[m_IndexIdCrops].f_Plant(p_Seed, p_Color);
        m_Seedbar.SetActive(false);

    }

    void OnDestroy()
    {
        OnCropsClickCallBack -= f_SeedBarActived;
        OnSeedClickCallBack -= f_OnSeedClick;
    }

    void f_UnLockCrops(int p_Index)
    {
        m_ConfirmationCrops.SetActive(true);
        m_IndexUnlock = p_Index;
    }

    public void f_DeclineConfirmation()
    {
        m_ConfirmationCrops.SetActive(false);
    }

    public void f_AcceptConfirmation()
    {
        Crops_GameObjec[m_IndexUnlock].m_Locked = false;
        Crops_GameObjec[m_IndexUnlock].m_LockedGameObject.SetActive(false);
        m_ConfirmationCrops.SetActive(false);
    }

    public void f_UseSkill(string p_SkillID)
    {
        Skill_Manager.m_Instance.f_UseSkill(p_SkillID);

    }

    public void f_BtnSkill(int p_level)
    {

        if (p_level >= 100)
            m_Btnsskill.SetActive(true);
    }
    public void f_Dbug()
    {
        Debug.Log("Tambah xp + 5000");
        Activity_Manager.m_Instance.m_Mastery.f_CalculateExp(50000);
    }
}
