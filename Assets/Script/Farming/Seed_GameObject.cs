using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Seed_GameObject : MonoBehaviour
{
    public int m_Id;
    public Image m_Seed;

    public void f_OnSeedClick()
    {
        Crops_Manager.m_Instance.OnSeedClickCallBack?.Invoke(m_Id, m_Seed.color);
    }
}
