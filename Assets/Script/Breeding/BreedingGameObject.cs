using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BreedingGameObject : MonoBehaviour
{
    // Start is called before the first frame update
    public string m_NodeName;
    public TextMeshProUGUI m_MobSizeText;
    public GameObject m_TheDoor;
    public GameObject m_Color;

    public int m_ID;
    public int m_MobSize;
    public bool m_Locked = true;
    public float m_MobSpeed = 5;
    public int m_TypeMob;
    float t_Speed;
    void Start()
    {
        f_Init();
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_Locked)
        {
            f_MobsProduction();
            f_Display();
        }
    }

    void f_MobsProduction()
    {
        t_Speed -= 1 * Time.deltaTime;
        if (t_Speed <= 0 && m_TypeMob != 0)
        {
            m_MobSize++;
            t_Speed = m_MobSpeed;
        }
    }

    public void f_BreedingUnLock()
    {
        m_Locked = false;
        m_TheDoor.SetActive(false);
    }

    void f_Init()
    {
        t_Speed = m_MobSpeed;
        m_TypeMob = 0;
    }


    public void f_Kill()
    {
        if (m_MobSize > 0 && m_Locked == false)
        {
            switch (Breeding_Station.m_Instance.m_TypeKill)
            {
                case 0:
                    Debug.Log("Daging Mentah Type" + m_TypeMob + " " + Breeding_Station.m_Instance.m_SizeKillMobs);
                    break;
                case 1:
                    Debug.Log("Daging Matang Type" + m_TypeMob + " " + Breeding_Station.m_Instance.m_SizeKillMobs);
                    break;
                case 2:
                    Debug.Log("Kepala Mobs Type" + m_TypeMob + " " + Breeding_Station.m_Instance.m_SizeKillMobs);
                    break;
            }

            m_MobSize -= Breeding_Station.m_Instance.m_SizeKillMobs;
            // Game_Manager.m_Instance.f_ItemDrops(gameObject.transform.position, "Mob", c_QuestGoal.e_Goaltype.Mob, 1);
            Activity_Manager.m_Instance.f_Action(m_NodeName);
            // Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Mob, Breeding_Station.m_Instance.m_SizeKillMobs);
            // Inventory.m_Instance.f_AddInventory("Mob");

            if (m_MobSize <= 0)
                m_MobSize = 0;
        }

    }


    public void f_TouchNode()
    {
        if (m_Locked == true)
        {
            Breeding_Station.m_Instance.m_TabSelectTypeMob.SetActive(false);
            Breeding_Station.m_Instance.m_TabConfirmation.SetActive(true);
            Breeding_Station.m_Instance.m_IdSelect = m_ID;
        }
        else if (m_TypeMob == 0)
        {
            Breeding_Station.m_Instance.m_IdSelect = m_ID;
            Breeding_Station.m_Instance.m_TabSelectTypeMob.SetActive(true);
        }
        else
        {
            Breeding_Station.m_Instance.m_PosBreedingGameObject = gameObject.transform;
            f_Kill();
        }
    }

    void f_Display()
    {
        if (m_MobSize != 0)
            m_MobSizeText.text = m_MobSize.ToString();
        else m_MobSizeText.text = "";
    }

}
