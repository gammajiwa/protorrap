using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enum;
public class Breeding_Station : MonoBehaviour
{
    public static Breeding_Station m_Instance;
    public GameObject m_TabConfirmation;
    public GameObject m_TabSelectTypeMob;
    public Transform m_ParentBreeding;
    public BreedingGameObject[] m_Breeding;
    public int m_SizeKillMobs;
    public int m_TypeKill;

    public Transform m_PosBreedingGameObject;

    public int m_IdSelect;
    private void Awake()
    {
        m_Instance = this;
    }

    private void Start()
    {
        m_Breeding = m_ParentBreeding.GetComponentsInChildren<BreedingGameObject>();


    }


    private void Update()
    {
        m_SizeKillMobs = (int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.KILL);
    }


    public void f_TypeKill(int p_index)
    {
        m_TypeKill = p_index;
    }


    public void f_Accept()
    {
        m_TabConfirmation.SetActive(false);
        m_Breeding[m_IdSelect].f_BreedingUnLock();
    }


    public void f_Decline()
    {
        m_TabConfirmation.SetActive(false);
    }


    public void f_SelectMob(int p_Index, Image p_Color)
    {
        m_Breeding[m_IdSelect].m_TypeMob = p_Index;
        m_TabSelectTypeMob.SetActive(false);
        m_Breeding[m_IdSelect].m_Color.GetComponent<Image>().color = p_Color.color;
    }

    public void f_CloseTabSelectType()
    {
        m_TabSelectTypeMob.SetActive(false);
    }

    public void f_UseSkill(string p_SkillID)
    {
        Skill_Manager.m_Instance.f_UseSkill(p_SkillID);

    }
}
