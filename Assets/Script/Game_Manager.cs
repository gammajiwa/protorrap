using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Enumerator;
using UnityEngine.SceneManagement;
using TMPro;
using System;
using Enum;

public class Game_Manager : MonoBehaviour
{
    public static Game_Manager m_Instance;
    public e_Activities m_GameActivity;
    public int m_PreScene;
    public List<Activity_Manager> m_Activities;
    public int m_IndexTutorial = 0;

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);

        SceneManager.LoadSceneAsync("UI", LoadSceneMode.Additive);
        m_IndexTutorial = 0;
    }

    public void f_Load(string p_Index, int p_)
    {
        if (!Player_Data.m_Instance.m_IsSleep)
        {
            StartCoroutine(f_LoadAsyn(p_Index));
            f_Activity(p_);
            UI_Manager.m_Instance.f_ActiveUI();
            UI_Manager.m_Instance.m_Locations.text = p_Index;
            UI_Manager.m_Instance.m_MoneyBar.SetActive(true);
            UI_Manager.m_Instance.m_LoadingScreen.SetActive(false);
        }
        m_PreScene = (int)m_GameActivity;
    }


    private void Start()
    {
        // offline Progression ads
        // if (!Player_Data.m_Instance.m_FirstLogin)
        //     AdScript.m_Instance.f_AddAds(e_TypeRewardAds.Progression);
        // if (m_GameActivity != e_Activities.Register)
        // {
        //     UI_Manager.m_Instance.m_Locations.text = m_GameActivity.ToString();
        // }
        m_PreScene = (int)m_GameActivity;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Tambah xp + 1250");
            Activity_Manager.m_Instance.m_Mastery.f_CalculateExp(50000);
        }
    }

    public void f_ChangeActivity(e_MasteryType p_MasteryType)
    {
        for (int i = 0; i < m_Activities.Count; i++)
        {
            if ((int)p_MasteryType == (int)m_Activities[i].m_Mastery.m_MasteryTypes[0])
            {
                Activity_Manager.m_Instance = m_Activities[i];
                break;
            }
        }
    }

    IEnumerator f_LoadAsyn(string p_Index)
    {
        UI_Manager.m_Instance.m_LoadingScreen.SetActive(true);
        SceneManager.UnloadSceneAsync(m_PreScene);
        AsyncOperation m_Opertator = SceneManager.LoadSceneAsync(p_Index, LoadSceneMode.Additive);
        while (!m_Opertator.isDone)
        {
            float m_Progress = Mathf.Clamp01(m_Opertator.progress / 0.9f);
            UI_Manager.m_Instance.m_LoadSlide.value = m_Progress;
            yield return null;
        }
    }

    public void f_OpenTabGame(int p_Index)
    {


        switch (p_Index)
        {
            case 1:
                if (!Player_Data.m_Instance.m_FirstLogin)
                    UI_Manager.m_Instance.m_Map.SetActive(!UI_Manager.m_Instance.m_Map.activeSelf);
                f_Temp();
                break;
            case 2:
                if (!Player_Data.m_Instance.m_FirstLogin)
                    UI_Manager.m_Instance.m_SettingGame.SetActive(!UI_Manager.m_Instance.m_SettingGame.activeSelf);
                break;
            case 3:
                if (!Player_Data.m_Instance.m_FirstLogin)
                    UI_Manager.m_Instance.m_AccountTab.SetActive(!UI_Manager.m_Instance.m_AccountTab.activeSelf);
                break;
            case 4:
                if (!Player_Data.m_Instance.m_FirstLogin)
                    UI_Manager.m_Instance.m_IAPShop.SetActive(!UI_Manager.m_Instance.m_IAPShop.activeSelf);
                break;
            case 5:
                if (!Player_Data.m_Instance.m_FirstLogin)
                    UI_Manager.m_Instance.m_FoodPedia.SetActive(!UI_Manager.m_Instance.m_FoodPedia.activeSelf);
                break;
            case 6:
                if (!Player_Data.m_Instance.m_FirstLogin)
                {
                    UI_Manager.m_Instance.m_AdsReward.SetActive(!UI_Manager.m_Instance.m_AdsReward.activeSelf);
                    Player_Data.m_Instance.m_TimerAds5Minute = 300;
                }
                break;
            case 7:
                if (!Player_Data.m_Instance.m_FirstLogin)
                    UI_Manager.m_Instance.m_RupyShop.SetActive(!UI_Manager.m_Instance.m_RupyShop.activeSelf);
                break;
            case 8:
                UI_Manager.m_Instance.m_Store.SetActive(!UI_Manager.m_Instance.m_Store.activeSelf);
                Inventory.m_Instance.f_CheckItem();
                Market.m_Instance.f_Init();
                break;

        }
    }



    public void f_PopUpFoodex(string p_Title)
    {
        Debug.Log("jalan");
        GameObject Foodex = Instantiate(UI_Manager.m_Instance.m_FoodexUI, UI_Manager.m_Instance.m_PosPopUp.transform.position, Quaternion.identity);
        Foodex.transform.parent = UI_Manager.m_Instance.m_PosPopUp.transform;
        Foodex.GetComponentInChildren<TextMeshProUGUI>().text = "[" + p_Title + "]" + "telah di tambahkan di Foodex";
        Destroy(Foodex, 2f);
    }




    // buat drop item sekalian beresin Quest sekalian timer
    public void f_ItemDrops(Vector3 p_Pos, string p_Name, c_QuestGoal.e_Goaltype p_Type, int p_Amount)
    {
        for (int i = 0; i < p_Amount; i++)
        {
            GameObject t_Item = Instantiate(UI_Manager.m_Instance.m_ItemDrop, p_Pos, Quaternion.identity);
            StartCoroutine(f_Timer(p_Name, p_Type));
            Destroy(t_Item, 2);

        }
    }


    void f_GetItems(string p_Name)
    {
        Inventory.m_Instance.f_AddInventory(p_Name);
    }

    void f_CheckQuest(c_QuestGoal.e_Goaltype p_Type)
    {
        Quest_Manager.m_Instance.f_CheckQuestIsDone(p_Type, 1);
    }

    IEnumerator f_Timer(string p_Name, c_QuestGoal.e_Goaltype p_Type)
    {
        yield return new WaitForSeconds(1.5f);
        f_GetItems(p_Name);
        f_CheckQuest(p_Type);
    }


    public void f_Activity(int p_Index)
    {
        switch (p_Index)
        {
            case 0:
                m_GameActivity = e_Activities.Register;
                break;
            case 1:
                m_GameActivity = e_Activities.Mining;
                f_ChangeActivity(e_MasteryType.MINING);
                break;
            case 2:
                m_GameActivity = e_Activities.Carpentry;
                f_ChangeActivity(e_MasteryType.CARPENTRY);
                break;
            case 3:
                m_GameActivity = e_Activities.Fishing;
                f_ChangeActivity(e_MasteryType.FISHING);
                break;
            case 4:
                m_GameActivity = e_Activities.Farming;
                f_ChangeActivity(e_MasteryType.FARMING);
                break;
            case 5:
                m_GameActivity = e_Activities.Breeding;
                f_ChangeActivity(e_MasteryType.BREEDING);
                break;
            case 6:
                m_GameActivity = e_Activities.Market;
                break;
            case 7:
                m_GameActivity = e_Activities.Home;
                break;

        }
    }
    public void f_PlayerSleep()
    {

        if (Player_Data.m_Instance.m_IsSleep)
        {
            Player_Data.m_Instance.m_IsSleep = false;

        }
        else
        {
            Player_Data.m_Instance.m_IsSleep = true;
            if (!Player_Data.m_Instance.m_FirstLogin)
            {
                AdScript.m_Instance.f_AddAds(0);
            }
        }
    }


    public void f_AddReceh(int p_Amount)
    {
        Player_Data.m_Instance.m_RecehCurrency += p_Amount;
        UI_Manager.m_Instance.f_UpdateCurrency();
        Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Gold, p_Amount);

    }


    void f_Temp()
    {
        GameObject[] m_Active = GameObject.FindGameObjectsWithTag("idrop");
        foreach (var item in m_Active)
        {
            Destroy(item);
        }

    }
    public void f_Atc()
    {

    }

}
