using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class c_QuestGoal
{
    public e_Goaltype m_GoalType;
    public int m_RequiredAmount;
    public int m_CurrentAmount;


    public bool f_IsReached()
    {
        return (m_CurrentAmount >= m_RequiredAmount);
    }


    public void f_QuestProgress(e_Goaltype p_Type, int m_Amount)
    {

        if (m_GoalType == p_Type)
        {
            m_CurrentAmount += m_Amount;
        }



        // switch ((int)p_Type)
        // {
        //     case 0:
        //         m_CurrentAmount++;
        //         break;
        //     case 1:
        //         m_CurrentAmount++;
        //         break;
        //     case 2:
        //         m_CurrentAmount++;
        //         break;
        //     case 3:
        //         m_CurrentAmount++;
        //         break;
        //     case 4:
        //         m_CurrentAmount++;
        //         break;
        //     case 5:
        //         m_CurrentAmount++;
        //         break;
        //     case 6:
        //         m_CurrentAmount++;
        //         break;
        //     case 7:
        //         m_CurrentAmount++;
        //         break;
        // }

    }


    public enum e_Goaltype
    {
        Mineral = 0,
        Logs,
        Fish,
        Crops,
        Mob,
        Sell,
        Gold,
        Energy


    }

}
