using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class c_Quest
{
    public bool m_IsActive;
    public string m_Title;
    public string m_Descriptions;

    public c_QuestGoal m_Goal;

    public void f_Complete()
    {
        m_IsActive = false;
    }

}
