using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class Quest_Manager : MonoBehaviour
{
    public static Quest_Manager m_Instance;
    public c_Quest[] m_Quest;
    public c_Quest m_QuestActive;


    private void Awake()
    {
        m_Instance = this;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        foreach (c_Quest item in m_Quest)
        {
            if (item.m_IsActive)
            {
                UI_Manager.m_Instance.m_QuestGameObject.SetActive(true);
                m_QuestActive = item;
                UI_Manager.m_Instance.m_DesQuestText.text = item.m_Descriptions;
                UI_Manager.m_Instance.m_ProgressQuestText.text = "( " + item.m_Goal.m_CurrentAmount + " / " + item.m_Goal.m_RequiredAmount + " )";
            }
        }
    }

    public void f_ActiveQuest(string p_Name)
    {
        c_Quest m_Active = Array.Find(m_Quest, c_Quest => c_Quest.m_Title == p_Name);
        m_Active.m_IsActive = true;
    }


    public void f_CheckQuestIsDone(c_QuestGoal.e_Goaltype p_Type, int m_Amount)
    {
        if (m_QuestActive.m_IsActive)
        {
            m_QuestActive.m_Goal.f_QuestProgress(p_Type, m_Amount);
            if (m_QuestActive.m_Goal.f_IsReached())
            {
                m_QuestActive.f_Complete();
                UI_Manager.m_Instance.m_QuestGameObject.SetActive(false);

                // buat tutorial
                if (Player_Data.m_Instance.m_FirstLogin)
                {
                    Debug.Log("lanjut jalan");
                    Tutorial_Manager.m_Instance.f_ContinueTutorial();
                }
            }
        }
    }


}
