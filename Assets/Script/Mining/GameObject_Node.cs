using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumerator;
using Enum;


public class GameObject_Node : MonoBehaviour
{
    public string m_NodeName;
    public static GameObject_Node m_Instance;
    public int m_MaxHP;
    public float m_Speed;
    public float m_Distance;
    public Health_Bar_Node m_HealthBar;
    public e_TypeNode m_TypeNode;

    int m_Hp;
    Transform m_Anchor;

    private void OnEnable()
    {
        m_Instance = this;
    }

    void Start()
    {
        f_Init();
        m_Anchor = GameObject.FindGameObjectWithTag("anchor").GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
        m_HealthBar.f_HealthBar(m_Hp, m_MaxHP);
        if (f_Miningable())
        {
            gameObject.transform.position = Vector3.MoveTowards(transform.position, m_Anchor.position, m_Speed * Time.deltaTime);
        }
        else
        {
            if (Tutorial_Manager.m_Instance.m_Tutorial == false && Input.GetMouseButtonDown(1))
            {
                f_Damage((int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.DAMAGE));
            }
        }
    }

    public void f_Init()
    {
        m_Hp = m_MaxHP;
        m_HealthBar.f_HealthBar(m_Hp, m_MaxHP);

    }

    public void f_Damage(int p_Damage)
    {
        m_Hp -= p_Damage;
        f_DestroyNode();
    }

    void f_DestroyNode()
    {
        if (m_Hp <= 0)
        {

            //tutorial & Quest
            switch ((int)m_TypeNode)
            {
                case 0:
                    // Inventory.m_Instance.f_AddInventory("Mineral");
                    // Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Mineral);
                    //Game_Manager.m_Instance.f_ItemDrops(gameObject.transform.position, "Mineral", c_QuestGoal.e_Goaltype.Mineral);
                    Activity_Manager.m_Instance.f_Action();
                    break;
                case 1:
                    // Inventory.m_Instance.f_AddInventory("Logs");
                    // Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Logs);
                    //Game_Manager.m_Instance.f_ItemDrops(gameObject.transform.position, "Logs", c_QuestGoal.e_Goaltype.Logs);
                    Activity_Manager.m_Instance.f_Action(m_NodeName);
                    break;
            }
            Debug.Log("Dapet Sesuatu");
            gameObject.SetActive(false);
            f_Init();
            Spawn_Node.m_Instance.OnNodeDestroyNodeCallBack?.Invoke();
        }
    }

    bool f_Miningable()
    {
        if (Vector3.Distance(transform.position, m_Anchor.transform.position) >= .1f)
            return true;
        return false;
    }


}

