using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health_Bar_Node : MonoBehaviour
{
    public Slider m_Slide;
    public Vector3 m_OffSite;


    // Update is called once per frame
    public void f_HealthBar(float p_HP, float p_MaxHP)
    {
        m_Slide.value = p_HP;
        m_Slide.maxValue = p_MaxHP;
    }
    void Update()
    {
        m_Slide.transform.position = Camera.main.WorldToScreenPoint(transform.parent.position + m_OffSite);
    }
}
