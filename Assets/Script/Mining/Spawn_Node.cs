using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumerator;
using Enum;

public class Spawn_Node : MonoBehaviour
{
    public static Spawn_Node m_Instance;
    public Transform m_Spawner;
    public delegate void OnNodeDestroyNode();
    public OnNodeDestroyNode OnNodeDestroyNodeCallBack;
    public GameObject[] m_Btnsskill;

    public e_Activities m_Activities;

    public string[] m_NameLog;

    int m_Intro;

    int m_Randome;

    private void Awake()
    {
        m_Instance = this;
    }
    void Start()
    {
        m_Intro = 0;
        OnNodeDestroyNodeCallBack += SpawnNode;
    }

    void Update()
    {
        f_BtnSkill();
        if (m_Intro == 0)
        {
            SpawnNode();
            m_Intro++;
        }
        else
            return;

    }

    void SpawnNode()
    {
        switch (Game_Manager.m_Instance.m_GameActivity)
        {
            case Enumerator.e_Activities.Mining:
                ObjectPool.m_Instance.f_SpanwFromPool("Mineral", m_Spawner.position, m_Spawner.rotation);
                break;
            case Enumerator.e_Activities.Carpentry:
                m_Randome = Random.Range(0, m_NameLog.Length);
                ObjectPool.m_Instance.f_SpanwFromPool(m_NameLog[m_Randome], m_Spawner.position, m_Spawner.rotation);
                break;
        }
    }

    public void f_UseSkill(string p_SkillID)
    {
        Skill_Manager.m_Instance.f_UseSkill(p_SkillID);

    }

    public void f_BtnSkill()
    {
        if (m_Activities == e_Activities.Carpentry)
        {
            if (Carpentry_Manager.m_Instance.m_Mastery.m_Level >= 100)
                m_Btnsskill[0].SetActive(true);

            if (Carpentry_Manager.m_Instance.m_Mastery.m_Level >= 100)
                m_Btnsskill[1].SetActive(true);
        }
        if (m_Activities == e_Activities.Mining)
        {
            if (Mining_Manager.m_Instance.m_Mastery.m_Level >= 100)
                m_Btnsskill[0].SetActive(true);

            if (Mining_Manager.m_Instance.m_Mastery.m_Level >= 450)
                m_Btnsskill[2].SetActive(true);
            if (Mining_Manager.m_Instance.m_Mastery.m_Level >= 300)
                m_Btnsskill[1].SetActive(true);
        }
    }
    public void f_Atc()
    {
        GameObject_Node.m_Instance.f_Damage((int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.DAMAGE));
    }

    public void f_Dbug()
    {
        Debug.Log("Tambah xp + 5000");
        Activity_Manager.m_Instance.m_Mastery.f_CalculateExp(50000);
    }
}



