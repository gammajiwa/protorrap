using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumerator;
using TMPro;
using Enum;
public class Fishing : MonoBehaviour
{
    public static Fishing m_Instance;
    public float m_MaxTimer;
    public GameObject m_NoticeInfo;
    public GameObject m_Bait;
    [HideInInspector]
    public float m_TimerGetFish;
    public bool m_Fishing = false;

    public GameObject[] m_Btnsskill;


    private void Awake()
    {
        m_Instance = this;
    }

    private void Start()
    {
        // foreach (var item in m_Btnsskill)
        // {
        //     item.SetActive(false);
        // }
    }
    void Update()
    {
        if (Game_Manager.m_Instance.m_GameActivity == e_Activities.Fishing && Tutorial_Manager.m_Instance.m_Tutorial == false)
        {
            if (m_Fishing)
            {
                if (m_TimerGetFish < (int)PlayerAttribute.m_Instance.f_GetFinalAttribute(e_AttributeType.TIMER))
                    m_TimerGetFish += Time.deltaTime;

            }
            else
                f_Init();

            if (f_TimerIsComplete())
            {
                m_NoticeInfo.GetComponent<TextMeshProUGUI>().text = "Tangkap Ikan";
                m_NoticeInfo.SetActive(true);
                StartCoroutine(f_LooseFishTimer());
            }
        }

        f_BtnSkill(Fishing_Manager.m_Instance.m_Mastery.m_Level);

    }

    public void f_Init()
    {
        m_TimerGetFish = 0;
        StopAllCoroutines();
        m_NoticeInfo.SetActive(false);
        m_Fishing = false;
    }

    public bool f_TimerIsComplete()
    {
        return m_TimerGetFish >= m_MaxTimer;
    }

    IEnumerator f_LooseFishTimer()
    {
        yield return new WaitForSeconds(2f);
        f_Init();

    }



    public void f_BaitTab()
    {
        m_Bait.SetActive(!m_Bait.activeSelf);
    }
    public void f_UseSkill(string p_SkillID)
    {
        Skill_Manager.m_Instance.f_UseSkill(p_SkillID);

    }


    public void f_BtnSkill(int p_level)
    {

        if (p_level >= 75)
            m_Btnsskill[0].SetActive(true);

        if (p_level >= 150)
            m_Btnsskill[1].SetActive(true);

        if (p_level >= 500)
            m_Btnsskill[2].SetActive(true);

    }
    public void f_Dbug()
    {
        Debug.Log("Tambah xp + 5000");
        Activity_Manager.m_Instance.m_Mastery.f_CalculateExp(50000);
    }
}
