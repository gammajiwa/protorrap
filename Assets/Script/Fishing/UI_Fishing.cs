using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Fishing : MonoBehaviour
{
    public Image m_BarFish;


    void Update()
    {
        m_BarFish.fillAmount = Fishing.m_Instance.m_TimerGetFish / Fishing.m_Instance.m_MaxTimer;
    }

    public void f_ButtonFish()
    {
        if (Fishing.m_Instance.f_TimerIsComplete())
        {
            Fishing.m_Instance.f_Init();
            Debug.Log("Dapat ikan");
            //Game_Manager.m_Instance.f_ItemDrops(new Vector3(0, -4, 0), "Fish", c_QuestGoal.e_Goaltype.Fish);
            Activity_Manager.m_Instance.f_Action();
            // Inventory.m_Instance.f_AddInventory("Fish");

            //tutorial & Quest
            // if (Quest_Manager.m_Instance.m_QuestActive.m_IsActive)
            // {
            //     Quest_Manager.m_Instance.m_QuestActive.m_Goal.f_QuestProgress(c_QuestGoal.e_Goaltype.Fish);
            //     if (Quest_Manager.m_Instance.m_QuestActive.m_Goal.f_IsReached())
            //     {
            //         Tutorial_Manager.m_Instance.f_ContinueTutorial();
            //         Quest_Manager.m_Instance.m_QuestActive.f_Complete();
            //         Quest_Manager.m_Instance.m_QuestGameObject.SetActive(false);
            //     }
            // }
            // Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Fish);
        }
        else
        {
            if (!Fishing.m_Instance.m_Fishing)
                Fishing.m_Instance.m_Fishing = true;
            else
                Fishing.m_Instance.f_Init();

        }
    }
}
