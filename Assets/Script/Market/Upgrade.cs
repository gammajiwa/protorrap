using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Upgrade
{
    public int m_Price;
    public List<LootDatabase_Scriptable> m_RequiredItem;
    public int[] m_Size;



}
