using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Enumerator;
public class Market : MonoBehaviour
{
    public static Market m_Instance;
    public GameObject m_TabSell;
    public GameObject m_TabUpgrade;
    public LootDatabase_Scriptable m_ItemSell;
    public TextMeshProUGUI m_TittleShop;

    public Image m_ProfileImage;
    public Transform m_Parent;
    public Item[] m_Items;
    public int m_IDMarket;
    public int m_IndexItemInventory;
    public bool m_ItemPick;



    private void Awake()
    {
        m_Instance = this;
        m_TabSell.SetActive(true);
        m_TabUpgrade.SetActive(false);

    }



    void Start()
    {
        m_Items = m_Parent.GetComponentsInChildren<Item>();
        f_UpdateUiInventory(Inventory.m_Instance.m_ItemsInInventory);

    }

    public void f_TabSell()
    {
        m_TabSell.SetActive(true);
        m_TabUpgrade.SetActive(false);
        Inventory.m_Instance.f_CheckItem();
    }

    public void f_TabUpgrade()
    {
        m_TabSell.SetActive(false);
        m_TabUpgrade.SetActive(true);
        Inventory.m_Instance.f_CheckItem();
    }


    public void f_ItemsShort(int p_Type)
    {

        Inventory.m_Instance.m_ItemsShort.Clear();

        foreach (LootDatabase_Scriptable item in Inventory.m_Instance.m_ItemsInInventory)
        {
            if ((int)item.m_Type == p_Type)
            {
                Inventory.m_Instance.m_ItemsShort.Add(item);
            }
            f_UpdateUiInventory(Inventory.m_Instance.m_ItemsShort);
        }

    }

    public void f_UpdateUiInventory(List<LootDatabase_Scriptable> p_Item)
    {
        for (int i = 0; i < m_Items.Length; i++)
        {
            if (i < p_Item.Count)
            {
                m_Items[i].f_ItemDisplay(p_Item[i]);
            }
            else
            {
                m_Items[i].f_ClearDisplay();
                // Inventory.m_Instance.f_DeleteItem();
            }
        }
    }




    public void f_DetailItem(LootDatabase_Scriptable p_Index)
    {
        m_ItemSell = p_Index;
        m_ProfileImage.sprite = m_ItemSell.m_LootImage;
        m_ItemPick = true;
        for (int i = 0; i < Inventory.m_Instance.m_ItemsInInventory.Count; i++)
        {
            if (Inventory.m_Instance.m_ItemsInInventory[i].m_LootName == m_ItemSell.m_LootName)
            {
                m_IndexItemInventory = i;
                return;
            }
        }
    }

    public void f_SellItem()
    {
        if (m_ItemPick)
        {
            if (m_ItemSell.m_Size > 0)
            {
                m_ItemSell.m_Size--;
                Game_Manager.m_Instance.f_AddReceh(m_ItemSell.m_Price);
                f_ItemsShort(m_IDMarket);
                Debug.Log("Jual Barang Receh + " + m_ItemSell.m_Price);
                Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Sell, 1);
                if (m_ItemSell.m_Size < 1)
                    f_Init();
            }
            else
            {
                Inventory.m_Instance.f_CheckItem();
                m_ItemSell = null;
            }

            // if (Inventory.m_Instance.m_SizeItem[m_IndexItemInventory] > 0)
            // {
            //     Inventory.m_Instance.m_SizeItem[m_IndexItemInventory]--;
            //     Game_Manager.m_Instance.f_AddReceh(m_ItemSell.m_Price);
            //     f_ItemsShort(m_IDMarket);
            //     Debug.Log("Jual Barang Receh + " + m_ItemSell.m_Price);
            //     Quest_Manager.m_Instance.f_CheckQuestIsDone(c_QuestGoal.e_Goaltype.Sell, 1);
            //     if (Inventory.m_Instance.m_SizeItem[m_IndexItemInventory] < 1)
            //         f_Init();
            // }
            // else
            // {
            //     Inventory.m_Instance.f_CheckItem();
            //     m_ItemSell = null;
            // }
        }
    }

    public void f_Init()
    {
        m_ItemPick = false;
        m_ProfileImage.sprite = null;
    }


    public void f_TittleMarket(int p_Index)
    {
        switch (p_Index)
        {
            case 0:
                m_TittleShop.text = "Mining Shop";
                break;
            case 1:
                m_TittleShop.text = "Carpentry Shop";
                break;
            case 2:
                m_TittleShop.text = "Fish Shop";
                break;
            case 3:
                m_TittleShop.text = "Farming Shop";
                break;
            case 4:
                m_TittleShop.text = "Breeding Shop";
                break;

        }
    }

}
