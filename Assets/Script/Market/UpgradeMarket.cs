using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UpgradeMarket : MonoBehaviour
{
    public static UpgradeMarket m_Instnace;
    public List<Upgrade> c_Upgrade;
    public Image[] m_Images;
    public TextMeshProUGUI m_PriceText;
    public TextMeshProUGUI[] m_SizeText;
    public int m_Price;

    public int[] m_CurrItem;
    public bool IsUpgrade;

    public Button m_BtnUpgrade;


    private void Awake()
    {
        m_Instnace = this;
    }
    // Update is called once per frame
    void Update()
    {
        f_Display();
    }


    public void f_Display()
    {




        for (int i = 0; i < m_Images.Length; i++)
        {
            m_Images[i].sprite = null;
            m_SizeText[i].text = "";
        }

        m_Price = c_Upgrade[Market.m_Instance.m_IDMarket].m_Price;
        m_PriceText.text = "$ " + m_Price.ToString();
        for (int i = 0; i < c_Upgrade[Market.m_Instance.m_IDMarket].m_RequiredItem.Count; i++)
        {
            m_Images[i].sprite = c_Upgrade[Market.m_Instance.m_IDMarket].m_RequiredItem[i].m_LootImage;
            m_SizeText[i].text = c_Upgrade[Market.m_Instance.m_IDMarket].m_Size[i].ToString();
        }



        // for (int i = 0; i < m_CurrItem.Length; i++)
        // {
        //     // m_CurrItem[i] = c_Upgrade[Market.m_Instance.m_IDMarket].m_RequiredItem[i].m_Size;
        //     for (int c = 0; c < Inventory.m_Instance.m_ItemsInInventory.Count; c++)
        //     {//??
        //         if (Inventory.m_Instance.m_ItemsInInventory[c].m_LootName == c_Upgrade[Market.m_Instance.m_IDMarket].m_RequiredItem[i].m_LootName)
        //             m_CurrItem[i] = Inventory.m_Instance.m_SizeItem[c];

        //     }

        //     if (Player_Data.m_Instance.m_RecehCurrency >= m_Price)
        //     {
        //         //masih aneh
        //         if (m_CurrItem[0] >= c_Upgrade[Market.m_Instance.m_IDMarket].m_Size[0] && m_CurrItem[1] >= c_Upgrade[Market.m_Instance.m_IDMarket].m_Size[1])
        //         {
        //             IsUpgrade = true;
        //         }

        //     }
        //     else
        //     {
        //         IsUpgrade = false;
        //     }

        // }

        // if (IsUpgrade)
        //     m_BtnUpgrade.interactable = true;
        // else m_BtnUpgrade.interactable = false;


    }


    public void f_Upgrade()
    {
        Activity_Manager.m_Instance.m_Tools[0].f_Upgrade();
        Player_Data.m_Instance.m_RecehCurrency -= m_Price;
        UI_Manager.m_Instance.f_UpdateCurrency();
        for (int i = 0; i < Inventory.m_Instance.m_ItemsInInventory.Count; i++)
        {
            for (int c = 0; c < m_CurrItem.Length; c++)
            {
                if (Inventory.m_Instance.m_ItemsInInventory[i].m_LootName == c_Upgrade[Market.m_Instance.m_IDMarket].m_RequiredItem[c].m_LootName)
                {
                    Inventory.m_Instance.m_ItemsInInventory[i].m_Size -= m_CurrItem[c];
                    Inventory.m_Instance.f_CheckItem();
                    return;
                }
            }
        }

    }

    void f_SumItem()
    {

    }

}